/*
    Copyright (C) 2006-2021 Ben Kibbey <bjk@luxsci.net>

    This file is part of pwmd.

    Pwmd is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    Pwmd is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Pwmd.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
#include <grp.h>

#include "common.h"
#include "acl.h"
#include "rcfile.h"
#include "util-misc.h"
#include "util-string.h"
#include "mutex.h"
#include "mem.h"
#ifdef WITH_GNUTLS
#include "tls.h"
#endif

static char *
acl_get_proc (pid_t pid, gpg_error_t *rc)
{
#ifndef __linux__ // FIXME: portability (BSD kvm, solaris, etc)
  *rc = 0;
  return NULL;
#else
  char buf[64];
  char path[PATH_MAX];
  char *p;
  ssize_t n;

  snprintf (buf, sizeof (buf), "/proc/%lu/exe", (long)pid);
  n = readlink (buf, path, sizeof (path)-1);

  if (n == -1)
    {
      *rc = gpg_error_from_syserror ();
      log_write ("%s: %s", __FUNCTION__, gpg_strerror (*rc));
      return NULL;
    }

  *rc = 0;
  path[n] = 0;

  p = str_dup (path);
  if (!p)
    *rc = GPG_ERR_ENOMEM;

  return p;
#endif
}

/* Check for further matches of a pathname which may be negated. */
static int
acl_check_proc_dup (char **users, const char *path)
{
  char **p;
  int allowed = 0;

  for (p = users; p && *p; p++)
    {
      int not = 0;
      char *s = *p;

      if (*s == '!' || *s == '-')
        {
          not = 1;
          s++;
        }

      if (*s != '/')
        continue;

      if (!strcmp (s, path))
        allowed = !not;
    }

  return allowed;
}

static int
acl_check_proc (char **users, char *user, const char *path, int *have_path)
{
  char *p = user;

  if (*p == '!' || *p == '-')
    p++;

  if (*p != '/')
    return 2;

  *have_path = 1;
  if (!strcmp (user, path))
    return acl_check_proc_dup (users, user);

  return 0;
}

gpg_error_t
do_validate_peer (assuan_context_t ctx, const char *section,
		  assuan_peercred_t *peer, char **rpath)
{
  char **users;
  int allowed = 0;
  gpg_error_t rc;
  struct client_s *client = assuan_get_pointer (ctx);

  if (!client)
    return GPG_ERR_FORBIDDEN;

#ifdef WITH_GNUTLS
  if (client->thd->remote)
    return tls_validate_access (client, section);
#endif

  rc = assuan_get_peercred (ctx, peer);
  if (rc)
    return rc;

  users = config_get_list (section, "allowed");
  if (users)
    {
      for (char **p = users; !rc && *p; p++)
	{
          rc = acl_check_common(client, *p, (*peer)->uid, (*peer)->gid,
                                &allowed);
        }

      /* Skip getting process name from a UID that is not our own to prevent
       * an ENOENT error since most modern systems hide process details from
       * other UID's. Access will be allowed based on other tests (filesystem
       * ACL to the socket, configuration, etc). */
      if (allowed && !rc && (*peer)->uid == getuid ())
        {
          int exec_allowed = 0;
          char *path = acl_get_proc ((*peer)->pid, &rc);
          int have_path = 0;

          for (char **p = users; !rc && path && *p; p++)
            {
              exec_allowed = acl_check_proc (users, *p, path, &have_path);
              if (!rc && exec_allowed == 1)
                break;
            }

          if (rpath)
            *rpath = path;
          else
            xfree (path);

          if (have_path)
            allowed = exec_allowed == 1;
        }

      strv_free (users);
    }

  return allowed && !rc ? 0 : rc ? rc : GPG_ERR_FORBIDDEN;
}

/* Test if uid is a member of group 'name'. Invert when 'not' is true. */
#ifdef HAVE_GETGRNAM_R
static gpg_error_t
acl_check_group (const char *name, uid_t uid, gid_t gid, int not, int *allowed)
{
  char *buf;
  struct group gr, *gresult;
  size_t len = sysconf (_SC_GETGR_R_SIZE_MAX);
  int err;
  gpg_error_t rc = 0;

  if (len == -1)
    len = 16384;

  buf = xmalloc (len);
  if (!buf)
    return GPG_ERR_ENOMEM;

  err = getgrnam_r (name, &gr, buf, len, &gresult);
  if (!err && gresult)
    {
      if (gresult->gr_gid == gid)
        {
          xfree (buf);
          *allowed = !not;
          return 0;
        }

      for (char **t = gresult->gr_mem; !rc && *t; t++)
        {
          char *tbuf;
          struct passwd pw;
          struct passwd *result = get_pwd_struct (*t, 0, &pw, &tbuf, &rc);

          if (!rc && result && result->pw_uid == uid)
            {
              xfree (tbuf);
              *allowed = !not;
              break;
            }

          xfree (tbuf);
        }

      xfree (buf);
      return rc;
    }
  else if (err)
    rc = gpg_error_from_errno (err);

  xfree (buf);
  return rc ? rc : !gresult ? 0 : GPG_ERR_EACCES;
}
#else
static gpg_error_t
acl_check_group (const char *name, uid_t uid, gid_t gid, int not, int *allowed)
{
  struct group *gresult;
  gpg_error_t rc = 0;

  errno = 0;
  gresult = getgrnam (name);
  if (!errno && gresult && gresult->gr_gid == gid)
    {
      *allowed = !not;
      return 0;
    }
  else if (errno)
    rc = gpg_error_from_syserror ();
  else if (!gresult)
    return 0;

  for (char **t = gresult->gr_mem; !rc && *t; t++)
    {
      char *buf;
      struct passwd pw;
      struct passwd *result = get_pwd_struct (*t, 0, &pw, &buf, &rc);

      if (!rc && result && result->pw_uid == uid)
        {
          xfree (buf);
          *allowed = !not;
          break;
        }

      xfree (buf);
    }

  return rc;
}
#endif

gpg_error_t
peer_is_invoker(struct client_s *client)
{
  struct invoking_user_s *user;
  int allowed = 0;

  if (client->thd->state == CLIENT_STATE_UNKNOWN)
    return GPG_ERR_EACCES;

  for (user = invoking_users; user; user = user->next)
    {
#ifdef WITH_GNUTLS
      if (client->thd->remote)
        {
          if (user->type == INVOKING_TLS
              && !strcmp(client->thd->tls->fp, user->id))
            allowed = user->not ? 0 : 1;

          continue;
        }
#endif

      if (user->type == INVOKING_GID)
        {
          gpg_error_t rc = acl_check_group (user->id,
                                            client->thd->peer->uid,
                                            client->thd->peer->gid,
                                            user->not, &allowed);
          if (rc)
            return rc;
        }
      else if (user->type == INVOKING_UID && client->thd->peer->uid == user->uid)
        allowed = user->not ? 0 : 1;
    }

  return allowed ? 0 : GPG_ERR_EACCES;
}

#ifdef HAVE_GETGRNAM_R
gpg_error_t
acl_check_common (struct client_s *client, const char *user, uid_t uid,
                  gid_t gid, int *allowed)
{
  int not = 0;
  int rw = 0;
  int tls = 0;
  gpg_error_t rc = 0;

  if (!user || !*user)
    return 0;

  if (*user == '-' || *user == '!')
    not = 1;

  if (*user == '+') // not implemented yet
    rw = 1;

  if (*user == '#') // TLS fingerprint hash
    tls = 1;

  if (not || rw || tls)
    user++;

  if (tls)
    {
#ifdef WITH_GNUTLS
      if (client->thd->remote)
        {
          if (!strcasecmp (client->thd->tls->fp, user))
            *allowed = !not;
        }

      return 0;
#else
      (void)client;
      return 0;
#endif
    }
#ifdef WITH_GNUTLS
  else if (client->thd->remote) // Remote client with no FP in the ACL
    return 0;
#endif

  if (*user == '@') // all users in group
    return acl_check_group (user+1, uid, gid, not, allowed);
  else
    {
      char *buf;
      struct passwd pw;
      struct passwd *pwd = get_pwd_struct (user, 0, &pw, &buf, &rc);

      if (!rc && pwd && pwd->pw_uid == uid)
        *allowed = !not;

      xfree (buf);
    }

  return rc;
}
#else
gpg_error_t
acl_check_common (struct client_s *client, const char *user, uid_t uid,
                  gid_t gid, int *allowed)
{
  gpg_error_t rc = 0;
  int not = 0;
  int rw = 0;
  int tls = 0;

  if (!user || !*user)
    return 0;

  if (*user == '-' || *user == '!')
    not = 1;

  if (*user == '+') // not implemented yet
    rw = 1;

  if (*user == '#') // TLS fingerprint hash
    tls = 1;

  if (not || rw || tls)
    user++;

  if (tls)
    {
#ifdef WITH_GNUTLS
      if (client->thd->remote)
        {
          if (!strcasecmp (client->thd->tls->fp, user))
            *allowed = !not;
        }

      return 0;
#else
      return 0;
#endif
    }

  if (*user == '@') // all users in group
    return acl_check_group (user+1, uid, gid, not, allowed);
  else
    {
      char *buf;
      struct passwd pw;
      struct passwd *result = get_pwd_struct (user, 0, &pw, &buf, &rc);

      if (!rc && result && result->pw_uid == uid)
        *allowed = !not;

      xfree (buf);
    }

  return rc;
}
#endif

gpg_error_t
validate_peer (struct client_s *cl)
{
  gpg_error_t rc;
  char *path = NULL;

#ifdef WITH_GNUTLS
  if (cl->thd->remote)
    return tls_validate_access (cl, NULL);
#endif

  MUTEX_LOCK (&cn_mutex);
  pthread_cleanup_push (release_mutex_cb, &cn_mutex);
  rc = do_validate_peer (cl->ctx, "global", &cl->thd->peer, &path);
  pthread_cleanup_pop (1);
  log_write ("peer %s: path=%s, uid=%i, gid=%i, pid=%i, rc=%u",
             !rc ? _("accepted") : _("rejected"), path,
             cl->thd->peer->uid, cl->thd->peer->gid,
             cl->thd->peer->pid, rc);
  xfree (path);
  return rc;
}
