/*
    Copyright (C) 2018-2021 Ben Kibbey <bjk@luxsci.net>

    This file is part of pwmd.

    Pwmd is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    Pwmd is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Pwmd.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef BULK_H
#define BULK_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <assuan.h>
#include "util-string.h"

struct command_table_s
{
  const char *name;	// The protocol command name.
  gpg_error_t (*handler) (assuan_context_t, char *line);
  const char *help;	// Help text for this command.
  int ignore_startup;	// Do not run file_modified() for this command.
  int unlock;		// unlock the file mutex after validating the checksum
  unsigned flock_type;
  int inquire;		/* For use with BULK: whether this command inquires
                           data. Command data is normally passed directly to
                           the handler , but some commands
                           (OPEN/SAVE/GENKEY/PASSWD) may need to inquire data
                           as well. This flag is to limit the number of BULK
                           status messages in the BULK command to only those
                           that need it. It is an indicator to the client of
                           which command is inquiring the data. See
                           dispatch_bulk_commands(). */
};

struct sexp_s {
  char *tag;		// A tag or token name.
  size_t taglen;	// The length of .tag.
  char *data;		// Data associated with .tag.
  size_t datalen;	// The length of .data.
  void *user;		// User data associated with this tag and data.
  struct sexp_s **next;	// Next tag list.
};

struct bulk_cmd_s
{
  struct command_table_s *cmd;	// Associated command for the sexp tag.
  char *result;			// The command result (xfer_data()).
  size_t result_len;		// The length of the command result.
  int ran;			/* Whether this command should be included in
				   the returned BULK command result. */
  gpg_error_t rc;		// Result code for this command.
};

gpg_error_t bulk_parse_commands (struct sexp_s ***, const char *str,
                                 struct command_table_s **);
gpg_error_t bulk_build_result (struct sexp_s **, char **);
void bulk_free_list (struct sexp_s **);

#endif
