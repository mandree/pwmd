/*
    Copyright (C) 2018-2021 Ben Kibbey <bjk@luxsci.net>

    This file is part of pwmd.

    Pwmd is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    Pwmd is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Pwmd.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <ctype.h>
#include <string.h>
#include "bulk.h"
#include "mem.h"

void
bulk_free_list (struct sexp_s **list)
{
  unsigned i;

  for (i = 0; list && list[i]; i++)
    {
      struct bulk_cmd_s *c = list[i]->user;

      if (c)
        {
          xfree (c->result);
          xfree (c);
        }

      xfree (list[i]->tag);
      xfree (list[i]->data);
      bulk_free_list (list[i]->next);
      xfree (list[i]);
    }

  xfree (list);
}

#define SKIP_WS(s, size) do {	\
    while (s && isspace (*s))	\
        s++, (*size)--;		\
} while (0)

#define NUMBER(s, size, buf, len, rc) do {	\
    const char *e = s;				\
    len = 0;					\
    while (e && isdigit (*e))			\
      e++, len++;				\
    if (len >= sizeof (buf)-1)			\
      rc = GPG_ERR_INV_LENGTH;			\
    else if (len) {				\
        memcpy (buf, s, len);		 	\
        buf[len] = 0;				\
        if (*e != ':')				\
          rc = GPG_ERR_SYNTAX;			\
        else {					\
          s += len+1;				\
          *size -= len+1;			\
          e = NULL;				\
          len = strtoul (buf, (char **)&e, 10);	\
          if (e && *e)				\
            rc = GPG_ERR_INV_LENGTH;		\
        }					\
    }						\
} while (0)

/* Like a canonical s-expression but without an internal representation. Any
 * character is allowed.  The tags and data are NULL terminated but not
 * considered in the length structure members. */
static gpg_error_t
parse_sexp (struct sexp_s ***head, const char *str, size_t *size)
{
  const char *p;
  size_t t = 0;
  gpg_error_t rc = 0;

  for (p = str; p && *p;)
    {
      struct sexp_s **s, *cur;
      char buf[16];
      char *tag = NULL, *data = NULL;
      size_t taglen, datalen;

      if (*p == ')')
        {
          p++;
          (*size)--;
          SKIP_WS (p, size);
          return 0;
        }
      else if (isspace (*p))
        {
          p++;
          (*size)--;
          continue;
        }
      else if (*p == '(')
        {
          size_t n;

          if (!t)
            return GPG_ERR_INV_SEXP;

          p++;
          (*size)--;
          cur = (*head)[t-1];
          n = *size;
          rc = parse_sexp (&cur->next, p, size);
          if (rc)
            break;

          p += n - *size;
          continue;
        }

      SKIP_WS (p, size);
      if (!isdigit (*p))
        {
          rc = GPG_ERR_SEXP_INV_LEN_SPEC;
          break;
        }

      NUMBER (p, size, buf, taglen, rc);
      if (rc)
        break;

      if (taglen > *size-1) // -1 for closing parentheses
        {
          rc = GPG_ERR_INV_LENGTH;
          break;
        }

      if (!taglen)
        continue;

      tag = xmalloc ((taglen+1) * sizeof (char));
      if (!tag)
        {
          rc = GPG_ERR_ENOMEM;
          break;
        }

      memcpy (tag, p, taglen);
      tag[taglen] = 0;
      p += taglen;
      *size -= taglen;

      SKIP_WS (p, size);
      NUMBER (p, size, buf, datalen, rc);
      if (rc)
        {
          xfree (tag);
          break;
        }

      if (datalen > *size-1)
        {
          xfree (tag);
          rc = GPG_ERR_INV_LENGTH;
          break;
        }

      if (datalen)
        {
          data = xmalloc ((datalen+1) * sizeof (char));
          if (!data)
            {
              xfree (tag);
              rc = GPG_ERR_ENOMEM;
              break;
            }

          memcpy (data, p, datalen);
          data[datalen] = 0;
          p += datalen;
          *size -= datalen;
        }

      cur = xcalloc (1, sizeof (struct sexp_s));
      if (!cur)
        {
          xfree (tag);
          xfree (data);
          rc = GPG_ERR_ENOMEM;
          break;
        }

      cur->tag = tag;
      cur->taglen = taglen;
      cur->data = data;
      cur->datalen = datalen;

      s = xrealloc (*head, (t+2) * sizeof (struct sexp_s *));
      if (!s)
        {
          xfree (tag);
          xfree (data);
          xfree (cur);
          rc = GPG_ERR_ENOMEM;
          break;
        }

      s[t++] = cur;
      s[t] = NULL;
      *head = s;
    }

  if (!rc && (!*p || (*p && *p != ')')))
    rc = GPG_ERR_SYNTAX;

  return rc;
}

static gpg_error_t
validate_command (struct sexp_s *cur, struct command_table_s **commands)
{
  unsigned i;

  for (i = 0; commands[i]; i++)
    {
      if (!strcasecmp (cur->tag, "BULK"))
        return GPG_ERR_ASS_NESTED_COMMANDS;
      else if (!strcasecmp (cur->tag, "RESET")
               || !strcasecmp (cur->tag, "BYE"))
        return GPG_ERR_ASS_UNEXPECTED_CMD;

      if (!strcasecmp (cur->tag, commands[i]->name))
        {
          struct bulk_cmd_s *p = xcalloc (1, sizeof (struct bulk_cmd_s));

          if (!p)
            return GPG_ERR_ENOMEM;

          p->cmd = commands[i];
          cur->user = p;
          return 0;
        }
    }

  return GPG_ERR_UNKNOWN_COMMAND;
}

/* Parse the s-expression using pwmd's syntax of:
 *
 * (2:id<I>:<id> <P>:<prot><D>:[<data>]
 *     [2:rc<R>:<code>[|<code>[...]](2:id...) | 2:id...])
 *
 * Where <I> is an integer specifying the length of the Id name <id>, <P> the
 * length of the protocol command name <prot>, <D> is the length of <data>
 * passed to <proto>, <R> is optional and the length of the numbered string
 * <code> representing one or more pipe separated return codes of <prot>.
 *
 * When the return code of the command <prot> returns and matches <code>, the
 * new command sequence in parentheses following <code> begins. You can specify
 * multiple return codes and command branches for each command. Multiple return
 * codes are separated with a pipe '|' to simulate an if-this-or-that
 * expression for a command return value.
 */
static gpg_error_t
parse_cmds (struct sexp_s **list, struct command_table_s **commands)
{
  gpg_error_t rc = 0;
  unsigned i;

  for (i = 0; list && list[i]; i++)
    {
      struct sexp_s *cur = list[i];

      if (!memcmp (cur->tag, "rc", cur->taglen))
        goto validate_rc;

      cur = list[i];
      if (!memcmp (cur->tag, "id", cur->taglen))
        {
          if (cur->next)
            return GPG_ERR_SYNTAX;
        }
      else
        return GPG_ERR_SYNTAX;

      cur = list[++i];
      if (!cur)
        return GPG_ERR_SYNTAX;

      if (memcmp (cur->tag, "rc", cur->taglen))
        {
          rc = validate_command (cur, commands);
          if (rc)
            return rc;
        }

validate_rc:
      if (!memcmp (cur->tag, "rc", cur->taglen))
        {
          char *t;

          if (!cur->data)
            return GPG_ERR_SYNTAX;

          if (*cur->data == '|' || cur->data[cur->datalen-1] == '|')
            return GPG_ERR_SYNTAX;

          for (t = cur->data; t && *t; t++)
            {
              if (!isdigit (*t) && *t != '|')
                return GPG_ERR_SYNTAX;
            }

          rc = parse_cmds (list[i]->next, commands);
          if (rc)
            break;
          continue;
        }
    }

  return !list ? GPG_ERR_SYNTAX : rc;
}

gpg_error_t
bulk_parse_commands (struct sexp_s ***result, const char *str,
                     struct command_table_s **commands)
{
  gpg_error_t rc;
  size_t n = strlen (str)-1;

  if (*str != '(')
    return GPG_ERR_SYNTAX;

  rc = parse_sexp (result, str+1, &n);
  if (!rc && n)
    rc = GPG_ERR_SYNTAX;
  if (!rc)
    rc = parse_cmds (*result, commands);

  if (rc)
    bulk_free_list (*result);

  return rc;
}

static size_t
number_length (size_t n, char **result)
{
  char buf[32];

  snprintf (buf, sizeof (buf), "%zu", n);
  if (result)
    *result = str_dup (buf);

  return strlen (buf);
}

static gpg_error_t
recurse_bulk_build_result (struct sexp_s **sexp, char **str, size_t *size)
{
  unsigned i;
  gpg_error_t rc = 0;

  for (i = 0; sexp[i]; i++)
    {
      struct sexp_s *id = NULL;
      struct bulk_cmd_s *cur;
      size_t len, n;
      char *p;
      char *idlen = NULL, *rclen = NULL, *rcstr = NULL, *resultlen = NULL;

      if (!memcmp (sexp[i]->tag, "id", sexp[i]->taglen))
        id = sexp[i++];
      else if (!memcmp (sexp[i]->tag, "rc", sexp[i]->taglen))
        goto parse_rc;

      cur = sexp[i]->user;
      if (!cur || !cur->ran)
        continue;

      if (!id)
        return GPG_ERR_SYNTAX;

      len = n = *size;
      n += number_length (id->datalen, &idlen);
      n += id->datalen;
      n += number_length (cur->rc, &rclen);
      n += number_length (strlen (rclen), &rcstr);
      n += number_length (cur->result_len, &resultlen);
      n += cur->result_len;
      n += 11;

      p = xrealloc (*str, (n+1) * sizeof (char));
      if (!p || !idlen || !rclen || !rcstr || !resultlen)
        {
          xfree (idlen);
          xfree (rclen);
          xfree (rcstr);
          xfree (resultlen);
          rc = GPG_ERR_ENOMEM;
          break;
        }

      *size = n;
      *str = p;

      memcpy (&p[len], "2:id", 4);
      len += 4;
      memcpy (&p[len], idlen, strlen (idlen));
      len += strlen (idlen);
      xfree (idlen);
      memcpy (&p[len++], ":", 1);
      memcpy (&p[len], id->data, id->datalen);
      len += id->datalen;

      memcpy (&p[len], "2:rc", 4);
      len += 4;
      memcpy (&p[len], rcstr, strlen (rcstr));
      len += strlen (rcstr);
      xfree (rcstr);
      memcpy (&p[len++], ":", 1);
      memcpy (&p[len], rclen, strlen (rclen));
      len += strlen (rclen);
      xfree (rclen);

      memcpy (&p[len], resultlen, strlen (resultlen));
      len += strlen (resultlen);
      xfree (resultlen);
      memcpy (&p[len++], ":", 1);
      if (cur->result)
        memcpy (&p[len], cur->result, cur->result_len);

      len += cur->result_len;
      p[len] = 0;

parse_rc:
      if (sexp[i+1] && sexp[i+1]->next)
        {
          rc = recurse_bulk_build_result (sexp[i+1]->next, str, size);
          if (rc)
            break;
        }
    }

  return rc;
}

/* Creates a string representing the result of a bulk command list. Syntax:
 *
 *     (11:bulk-result 2:id<I>:<id> 2:rc<R>:<code> <D>:[<data>] [2:id...])
 *
 * Where <I> is the length of <id>, <R> is the length of the command result
 * <code> and <D> is the length of the command result <data> if any.
 */
gpg_error_t
bulk_build_result (struct sexp_s **sexp, char **result)
{
  gpg_error_t rc;
  size_t size = 0;

  *result = str_dup ("(11:bulk-result");
  if (!(*result))
    return GPG_ERR_ENOMEM;

  size = strlen (*result);
  rc = recurse_bulk_build_result (sexp, result, &size);
  if (!rc)
    {
      char *p = xrealloc (*result, (size+2) * sizeof (char));

      if (!p)
        {
          xfree (*result);
          return GPG_ERR_ENOMEM;
        }

      p[size++] = ')';
      p[size] = 0;
      *result = p;
    }
  else
    xfree (*result);

  return rc;
}
