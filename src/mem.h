/*
    Copyright (C) 2006-2021 Ben Kibbey <bjk@luxsci.net>

    This file is part of pwmd.

    Pwmd is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    Pwmd is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Pwmd.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef MEM_H
#define MEM_H
#include <stdlib.h>

#ifdef __cplusplus
extern "C"
{
#endif

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <sys/types.h>
#include <stdarg.h>

#ifdef MEM_DEBUG
#define xfree free
#define xmalloc malloc
#define xrealloc realloc
#define xcalloc calloc
  void *xrealloc_gpgrt (void *, size_t);
#else
  void xfree (void *ptr);
  void *xmalloc (size_t size);
  void *xrealloc (void *ptr, size_t size);
  void *xcalloc (size_t nmemb, size_t size);
  void *xrealloc_gpgrt (void *, size_t);
#endif
void wipememory (void *ptr, int c, size_t len);

#ifdef __cplusplus
}
#endif

#endif
