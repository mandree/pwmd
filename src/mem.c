/*
    Copyright (C) 2006-2021 Ben Kibbey <bjk@luxsci.net>

    This file is part of pwmd.

    Pwmd is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    Pwmd is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Pwmd.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stddef.h>

#include "mem.h"

void *
xrealloc_gpgrt (void *p, size_t n)
{
  if (n)
    {
      if (p)
        return xrealloc (p, n);

      return xmalloc (n);
    }

  xfree (p);
  return NULL;
}

/* Borrowed from g10code:
 * https://lists.gnupg.org/pipermail/gnupg-devel/2018-November/034060.html
 */
void
wipememory (void *ptr, int c, size_t len)
{
#if defined(HAVE_W32_SYSTEM) && defined(SecureZeroMemory)
  (void)c;
  SecureZeroMemory (ptr, len);
#elif defined(HAVE_EXPLICIT_BZERO)
  (void)c;
  explicit_bzero (ptr, len);
#elif defined(HAVE_MEMSET_S)
  memset_s (ptr, len, c, len);
#else
  /* Prevent compiler from optimizing away the call to memset by accessing
     memset through volatile pointer. */
  static void *(*volatile memset_ptr)(void *, int, size_t) = (void *)memset;
  memset_ptr (ptr, c, len);
#endif
}

#ifndef MEM_DEBUG
struct memchunk_s
{
  size_t size;
  char data[1];
} __attribute ((packed));

void
xfree (void *ptr)
{
  struct memchunk_s *m;
  void *p;

  if (!ptr)
    return;

  m = (struct memchunk_s *)((char *)ptr-(offsetof (struct memchunk_s, data)));
  p = (void *)((char *)m+(offsetof (struct memchunk_s, data)));
  wipememory (p, 0, m->size);
  free (m);
}

void *
xmalloc (size_t size)
{
  struct memchunk_s *m;

  if (!size)
    return NULL;

  m = malloc (sizeof (struct memchunk_s)+size);
  if (!m)
    return NULL;

  m->size = size;
  return (void *)((char *)m+(offsetof (struct memchunk_s, data)));
}

void *
xcalloc (size_t nmemb, size_t size)
{
  void *p;
  struct memchunk_s *m;

  m = malloc (sizeof (struct memchunk_s)+(nmemb*size));
  if (!m)
    return NULL;

  m->size = nmemb*size;
  p = (void *)((char *)m+(offsetof (struct memchunk_s, data)));
  memset (p, 0, m->size);
  return p;
}

void *
xrealloc (void *ptr, size_t size)
{
  void *p, *np;
  struct memchunk_s *m, *mp;
  size_t n;

  if (!size && ptr)
    {
      m = (struct memchunk_s *)((char *)ptr-(offsetof (struct memchunk_s, data)));
      p = (void *)((char *)m+(offsetof (struct memchunk_s, data)));
      wipememory (p, 0, m->size);
      free (m);
      return NULL;
    }
  else if (!ptr)
    return xmalloc (size);

  m = malloc (sizeof (struct memchunk_s)+size);
  if (!m)
    return NULL;

  m->size = size;
  np = (void *)((char *)m+(offsetof (struct memchunk_s, data)));

  mp = (struct memchunk_s *)((char *)ptr-(offsetof (struct memchunk_s, data)));
  p = (void *)((char *)mp+(offsetof (struct memchunk_s, data)));

  n = size > mp->size ? mp->size : size;
  memcpy (np, p, n);
  wipememory (p, 0, mp->size);

  free (mp);
  return (void *)((char *)m+(offsetof (struct memchunk_s, data)));
}

#endif
