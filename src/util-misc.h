/*
    Copyright (C) 2006-2021 Ben Kibbey <bjk@luxsci.net>

    This file is part of pwmd.

    Pwmd is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    Pwmd is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Pwmd.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef UTIL_MISC_H
#define UTIL_MISC_H

#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <wchar.h>
#include <pwd.h>
#include <netinet/in.h>
#include "rcfile.h"

#ifdef HAVE_BACKTRACE
#include <execinfo.h>
#include <syslog.h>
#define BACKTRACE_COUNT	20
#define BACKTRACE(fn)	do {						\
	int n, nptrs;							\
	char **strings;							\
	void *buffer[BACKTRACE_COUNT];					\
	nptrs = backtrace(buffer, BACKTRACE_COUNT);			\
	strings = backtrace_symbols(buffer, nptrs);			\
	for (n = 0; n < nptrs; n++)					\
	    log_write("BACKTRACE: (%s) %i: %s", fn, n, strings[n]);	\
    } while (0)
#endif

#define log_write0 log_write

#define log_write1(...) do { \
	if (log_level >= 1)                                     \
	    log_write(__VA_ARGS__);				\
    } while (0)

#define log_write2(...) do {					\
	if (log_level >= 2)                                     \
	    log_write(__VA_ARGS__);				\
    } while (0)

typedef enum
{
  OPTION_TYPE_NOARG,
  OPTION_TYPE_ARG,
  OPTION_TYPE_OPTARG,
} option_type_t;

struct argv_s
{
  const char *opt;
  option_type_t type;
  gpg_error_t (*func) (void *data, void *value);
};

typedef struct
{
  size_t len;
  void *buf;
} membuf_t;

extern char *home_directory;
extern int log_level;

gpg_error_t parse_options (char **line, struct argv_s *args[], void *data,
                           int more);
int valid_filename (const char *filename);
char *expand_homedir (char *str);
char *bin2hex (const unsigned char *data, size_t len);
char *gnupg_escape (const char *str);
char *strip_texi_and_wrap (const char *str, int html);
void free_key (void *data);
gpg_error_t create_thread (void *(*cb) (void *), void *data,
			   pthread_t * tid, int detached);
void release_mutex_cb (void *arg);
void close_fd_cb (void *arg);
gpg_error_t get_checksum (const char *filename, unsigned char **r_crc,
                          size_t * r_crclen);
gpg_error_t get_checksum_memory (void *data, size_t size,
                                 unsigned char **r_crc, size_t *r_crclen);
char *get_username (uid_t);
char *get_home_dir ();
struct passwd *get_pwd_struct (const char *user, uid_t uid, struct passwd *,
                               char **buf, gpg_error_t *);
wchar_t *str_to_wchar (const char *str);
void *get_in_addr (struct sockaddr *sa);
gpg_error_t open_check_file (const char *, int *, struct stat *, int);

#endif
