/*
    Copyright (C) 2006-2021 Ben Kibbey <bjk@luxsci.net>

    This file is part of pwmd.

    Pwmd is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    Pwmd is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Pwmd.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <pthread.h>
#include <stdarg.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <poll.h>

#ifdef HAVE_LINUX_SOCKIOS_H
#include <linux/sockios.h>
#endif

#ifdef HAVE_SYS_FILIO_H
#include <sys/filio.h>
#endif

#include "pwmd-error.h"
#include "mutex.h"
#include "util-misc.h"
#include "common.h"
#include "util-string.h"
#include "status.h"
#include "cache.h"
#include "mem.h"

gpg_error_t
send_status (assuan_context_t ctx, status_msg_t which, const char *fmt, ...)
{
  const char *line = NULL;
  char buf[ASSUAN_LINELENGTH + 1];
  const char *status = NULL;
#ifdef WITH_GNUTLS
  struct client_s *client = ctx ? assuan_get_pointer (ctx) : NULL;
#endif
  gpg_error_t rc = 0;

  if (fmt)
    {
      va_list ap;

      va_start (ap, fmt);
      vsnprintf (buf, sizeof (buf), fmt, ap);
      va_end (ap);
      line = buf;
    }

  switch (which)
    {
#ifdef WITH_GNUTLS
    case STATUS_REHANDSHAKE:
      status = "REHANDSHAKE";
      break;
#endif
    case STATUS_EXPIRE:
      status = "EXPIRE";
      break;
    case STATUS_GENKEY:
      status = "GENKEY";
      break;
    case STATUS_XFER:
      status = "XFER";
      break;
    case STATUS_CACHE:
      snprintf (buf, sizeof (buf), "%u", cache_file_count ());
      line = buf;
      status = "CACHE";
      break;
    case STATUS_CLIENTS:
      MUTEX_LOCK (&cn_mutex);
      snprintf (buf, sizeof (buf), "%u", slist_length (cn_thread_list));
      line = buf;
      MUTEX_UNLOCK (&cn_mutex);
      status = "CLIENTS";
      break;
    case STATUS_LOCKED:
      status = "LOCKED";
      line = _("Waiting for lock");
      break;
    case STATUS_ENCRYPT:
      status = "ENCRYPT";
      break;
    case STATUS_DECRYPT:
      status = "DECRYPT";
      break;
    case STATUS_NEWFILE:
      status = "NEWFILE";
      break;
    case STATUS_KEEPALIVE:
      status = "KEEPALIVE";
      break;
    case STATUS_GPGME:
      status = "GPGME";
      break;
    case STATUS_STATE:
      status = "STATE";
      break;
    case STATUS_BULK:
      status = "BULK";
    }

  if (!ctx)
    {
      log_write ("%s %s", status, line ? line : "");
      return 0;
    }

#ifdef WITH_GNUTLS
  if (client && client->thd->remote && which == STATUS_KEEPALIVE)
    {
      int buffered = 0;

#ifdef HAVE_DECL_SIOCOUTQ
      if (ioctl (client->thd->fd, SIOCOUTQ, &buffered) == -1)
	log_write ("%s(%i): ioctl: %s", __FUNCTION__, __LINE__,
		   pwmd_strerror (gpg_error_from_errno (errno)));
#elif defined (HAVE_DECL_FIONWRITE)
      if (ioctl (client->thd->fd, FIONWRITE, &buffered) == -1)
	log_write ("%s(%i): ioctl: %s", __FUNCTION__, __LINE__,
		   pwmd_strerror (gpg_error_from_errno (errno)));
#elif !defined (HAVE_WINLIKE_SYSTEM)
      if (1)
	{
	  int sndbuf;
	  socklen_t len = sizeof(int);

	  if (getsockopt (client->thd->fd, SOL_SOCKET, SO_SNDBUF, &sndbuf,
			  &len) == -1)
	    log_write ("%s(%i): getsockopt: %s", __FUNCTION__, __LINE__,
		       pwmd_strerror (gpg_error_from_errno (errno)));
	  else
	    {
	      int lowat;

	      len = sizeof(int);
	      if (getsockopt (client->thd->fd, SOL_SOCKET, SO_SNDLOWAT,
			      &lowat, &len) == -1)
		log_write ("%s(%i): getsockopt: %s", __FUNCTION__,
			   __LINE__, pwmd_strerror (gpg_error_from_errno (errno)));
	      else
		{
		  len = sizeof(int);
		  if (setsockopt (client->thd->fd, SOL_SOCKET, SO_SNDLOWAT,
				  &sndbuf, len) == -1)
		    log_write ("%s(%i): setsockopt: %s", __FUNCTION__,
			       __LINE__,
			       pwmd_strerror (gpg_error_from_errno (errno)));
		  else
		    {
                      struct pollfd fds[1];
		      int n;

                      fds[0].fd = client->thd->fd;
                      fds[0].events = POLLOUT;

		      buffered = client->thd->last_buffer_size + 1;
                      n = poll (fds, 1, 0);
		      len = sizeof(int);
		      if (setsockopt (client->thd->fd, SOL_SOCKET, SO_SNDLOWAT,
				      &lowat, len) == -1)
			log_write ("%s(%i): setsockopt: %s", __FUNCTION__,
				   __LINE__,
				   pwmd_strerror (gpg_error_from_errno (errno)));
		      if (n > 0 && (fds[0].revents & POLLOUT))
			buffered = 0;
		    }
		}
	    }
	}
#endif
      if (buffered)
	{
	  int interval = config_get_integer ("global", "keepalive_interval");
	  int timeout = config_get_integer ("global", "tls_timeout");

	  if (buffered < client->thd->last_buffer_size)
	    client->thd->buffer_timeout = 0;

	  client->thd->last_buffer_size = buffered;

	  if (++client->thd->buffer_timeout * interval >= timeout)
	    rc = gpg_error (GPG_ERR_ETIMEDOUT);
	}
      else
	client->thd->buffer_timeout = client->thd->last_buffer_size = 0;
    }
#endif

  if (!rc)
    rc = assuan_write_status (ctx, status, line);

#ifdef WITH_GNUTLS
  if (client && client->thd->remote && which != STATUS_KEEPALIVE)
    client->thd->buffer_timeout = client->thd->last_buffer_size = 0;
#endif

  return rc;
}

static void
do_send_status_all (status_msg_t s, const char *line, pthread_t *not_tid)
{
  MUTEX_LOCK (&cn_mutex);
  int i = 0;
  int t = slist_length (cn_thread_list);

  pthread_cleanup_push (release_mutex_cb, &cn_mutex);

  for (; i < t; i++)
    {
      struct client_thread_s *thd = slist_nth_data (cn_thread_list, i);
      struct status_msg_s *msg, *p;
      char c = 0xff;
      int match = 0;
      gpg_error_t rc;

      if (not_tid && pthread_equal (*not_tid, thd->tid))
        continue;

      if (thd->state == CLIENT_STATE_UNKNOWN
          || thd->state == CLIENT_STATE_DISCON)
        continue;

      MUTEX_LOCK (&thd->status_mutex);
      if (s == STATUS_STATE && !thd->cl->client_state)
        {
          MUTEX_UNLOCK (&thd->status_mutex);
          continue;
        }

      pthread_cleanup_push (release_mutex_cb, &thd->status_mutex);

      for (p = thd->msg_queue; p; p = p->next)
	{
	  if (p->s == s)
	    {
	      match = 1;
	      break;
	    }
	}

      if (match && s != STATUS_STATE)
	{
          xfree (p->line);
          p->line = line ? str_dup (line) : NULL;

          if (!thd->wrote_status)
            {
              ssize_t ret = write (thd->status_msg_pipe[1], &c, 1);

              rc = gpg_error_from_syserror ();
              if (ret == -1)
                log_write ("%s (%i): %s", __FUNCTION__, __LINE__,
                           pwmd_strerror (rc));
            }

          thd->wrote_status = 1;
	}
      else
        {
          msg = xcalloc (1, sizeof (struct status_msg_s));
          msg->s = s;
          msg->line = line ? str_dup (line) : NULL;

          for (p = thd->msg_queue; p && p->next; p = p->next);
          if (!p)
            thd->msg_queue = msg;
          else
            p->next = msg;

          if (!thd->wrote_status)
            {
              ssize_t ret = write (thd->status_msg_pipe[1], &c, 1);

              rc = gpg_error_from_syserror ();
              if (ret == -1)
                log_write ("%s (%i): %s", __FUNCTION__, __LINE__,
                           pwmd_strerror (rc));
            }

          thd->wrote_status = 1;
        }

      pthread_cleanup_pop (1);
    }

  pthread_cleanup_pop (1);
}

void
send_status_all_not_self (status_msg_t s, const char *fmt, ...)
{
  char *line = NULL;
  pthread_t tid = pthread_self ();

  if (fmt)
    {
      va_list ap;

      va_start (ap, fmt);
      str_vasprintf (&line, fmt, ap);
      va_end (ap);
    }

  pthread_cleanup_push (xfree, line);
  do_send_status_all (s, line, &tid);
  pthread_cleanup_pop (1);
}

void
send_status_all (status_msg_t s, const char *fmt, ...)
{
  char *line = NULL;

  if (fmt)
    {
      va_list ap;

      va_start (ap, fmt);
      str_vasprintf (&line, fmt, ap);
      va_end (ap);
    }

  pthread_cleanup_push (xfree, line);
  do_send_status_all (s, line, NULL);
  pthread_cleanup_pop (1);
}
