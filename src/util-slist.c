/*
    Copyright (C) 2012-2021 Ben Kibbey <bjk@luxsci.net>

    This file is part of pwmd.

    Pwmd is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    Pwmd is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Pwmd.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include "util-slist.h"
#include "mem.h"

unsigned
slist_length (struct slist_s *list)
{
  struct slist_s *cur;
  unsigned n = 0;

  for (cur = list; cur; cur = cur->next)
    n++;

  return n;
}

void *
slist_nth_data (struct slist_s *list, unsigned n)
{
  struct slist_s *cur;
  unsigned i;

  for (i = 0, cur = list; cur; cur = cur->next, i++)
    {
      if (i == n)
	return cur->data;
    }

  return NULL;
}

struct slist_s *
slist_append (struct slist_s *list, void *data)
{
  struct slist_s *cur;

  if (!list)
    {
      cur = xcalloc (1, sizeof (struct slist_s));
      cur->data = data;
      return cur;
    }

  for (cur = list; cur; cur = cur->next)
    {
      if (!cur->next)
	break;
    }

  cur->next = xcalloc (1, sizeof (struct slist_s));
  cur->next->data = data;
  return list;
}

static struct slist_s *
free_once (struct slist_s *cur)
{
  struct slist_s *next = cur ? cur->next : NULL;

  xfree (cur);
  return next;
}

void
slist_free (struct slist_s *list)
{
  while (list)
    list = free_once (list);
}

struct slist_s *
slist_remove (struct slist_s *list, void *data)
{
  struct slist_s *prev, *cur;

  for (cur = prev = list; cur; prev = cur, cur = cur->next)
    {
      if (cur->data == data)
	{
	  struct slist_s *next = free_once (cur);

	  if (cur != list)
	    prev->next = next;
	  else
	    list = next;
	  break;
	}
    }

  return list;
}
