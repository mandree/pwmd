/*
    Copyright (C) 2012-2021 Ben Kibbey <bjk@luxsci.net>

    This file is part of pwmd.

    Pwmd is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    Pwmd is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Pwmd.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef UTIL_STRING_H
#define UTIL_STRING_H

#include <stdarg.h>

#ifdef HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif

struct string_s
{
  size_t allocated;
  size_t len;
  char *str;
};

// These mimic GLib's string utility functions.
void string_free (struct string_s *s, int with_data);
struct string_s *string_erase (struct string_s *s, ssize_t pos, ssize_t len);
struct string_s *string_new (const char *str);
struct string_s *string_new_content (char *str);
struct string_s *string_append (struct string_s *s, const char *str);
struct string_s *string_truncate (struct string_s *s, size_t n);
struct string_s *string_prepend (struct string_s *s, const char *str);
struct string_s *string_append_printf (struct string_s *s, const char *fmt,
				       ...);
struct string_s *string_insert_c (struct string_s *s, ssize_t pos, char c);

int strv_printf (char ***array, const char *fmt, ...);
void strv_free (char **str);
char **strv_cat (char **a, char *str);
char **strv_catv (char **dst, char **src);
int strv_length (char **a);
char **strv_dup (char **src);
char *strv_join (const char *delim, char **a);

char **str_split (const char *str, const char *delim, int count);
char **str_split_ws (const char *str, const char *delim, int count);
char *str_down (char *str);
char *str_chomp (char *str);
char *str_dup (const char *);
char *str_asprintf (const char *fmt, ...);
int str_vasprintf (char **result, const char *fmt, va_list ap);

#endif
