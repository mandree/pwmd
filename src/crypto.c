/*
    Copyright (C) 2006-2021 Ben Kibbey <bjk@luxsci.net>

    This file is part of pwmd.

    Pwmd is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    Pwmd is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Pwmd.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <pthread.h>

#ifdef WITH_LIBACL
#include <sys/acl.h>
#endif

#include "pwmd-error.h"
#include "util-misc.h"
#include "common.h"
#include "rcfile.h"
#include "crypto.h"
#include "cache.h"
#include "mem.h"
#include "util-string.h"
#include "rcfile.h"
#include "mutex.h"

static unsigned keepalive;
static pthread_mutex_t crypto_mutex = PTHREAD_MUTEX_INITIALIZER;

#define BUFSIZE 4096

#define STATUS_TIMEOUT_INIT(crypto) \
  do { \
      crypto->status_timeout = time (NULL); \
      crypto->progress_rc = 0; \
  } while (0)

#define STATUS_TIMEOUT_TEST(crypto, rc, s, line) \
  do { \
      time_t now = time (NULL); \
      if (now - crypto->status_timeout >= keepalive && crypto->client_ctx) { \
        rc = send_status (crypto->client_ctx, s, line); \
        crypto->status_timeout = now; \
      } \
  } while (0)

static gpg_error_t
get_password_file (const char *keyfile, unsigned char **result, size_t *len)
{
  struct stat st;
  int fd;
  unsigned char *buf = NULL;
  gpg_error_t rc = 0;

  log_write (_ ("obtaining passphrase from passphrase file"));
  rc = open_check_file (keyfile, &fd, &st, 0);
  if (rc)
    return rc;

  buf = xmalloc (st.st_size+1);
  if (buf)
    {
      size_t rlen = read (fd, buf, st.st_size);

      if (rlen != st.st_size)
        rc = GPG_ERR_ASS_READ_ERROR;
      else
        {
          buf[rlen] = 0;
          /* The passphrase may have been truncated do to a nul byte before the
           * EOF. Better to return an error here rather than passing the
           * truncated passphrase on. */
          if (rlen && strlen ((char *)buf) != rlen)
            rc = GPG_ERR_INV_PASSPHRASE;

          if (!rc)
            {
              *result = buf;
              *len = rlen;
            }
        }
    }
  else
    rc = GPG_ERR_ENOMEM;

  close (fd);
  if (rc)
    xfree (buf);

  return rc;
}

static gpgme_error_t
status_cb (void *data, const char *keyword, const char *args)
{
  struct crypto_s *crypto = data;

  // Cache pushing.
  if (!crypto->client_ctx)
    return 0;

  if (!strcmp (keyword, "INQUIRE_MAXLEN") &&
      (crypto->flags & CRYPTO_FLAG_KEYFILE))
    return 0;

  return assuan_write_status (crypto->client_ctx, keyword, args);
}

static gpgme_error_t
passphrase_cb (void *data, const char *hint, const char *info, int bad, int fd)
{
  struct crypto_s *crypto = data;
  unsigned char *result = NULL;
  size_t len;
  gpg_error_t rc = 0;

  if (crypto->flags & CRYPTO_FLAG_KEYFILE)
    rc = get_password_file (crypto->keyfile, &result, &len);
  else
    {
      if (hint)
        rc = assuan_write_status (crypto->client_ctx, "PASSPHRASE_HINT", hint);

      if (!rc && info)
        rc = assuan_write_status (crypto->client_ctx, "PASSPHRASE_INFO", info);

      if (!rc)
        {
          const char *keyword = "PASSPHRASE";

          if (!bad && crypto->flags & CRYPTO_FLAG_PASSWD_SIGN)
              keyword = "SIGN_PASSPHRASE";
          else if (!bad && crypto->flags & CRYPTO_FLAG_PASSWD_NEW)
            {
              crypto->flags |= CRYPTO_FLAG_PASSWD_SIGN;
              keyword = "NEW_PASSPHRASE";
            }
          else if (!bad && crypto->flags & CRYPTO_FLAG_PASSWD)
            crypto->flags |= CRYPTO_FLAG_PASSWD_NEW;
          else if (!bad && crypto->flags & CRYPTO_FLAG_NEWFILE)
            {
              keyword = "NEW_PASSPHRASE";
              if (crypto->save.sigkey)
                crypto->flags |= CRYPTO_FLAG_PASSWD_SIGN;
            }

          rc = assuan_inquire (crypto->client_ctx, keyword, &result, &len, 0);
        }
    }

  if (!rc)
    {
      pthread_cleanup_push ((void *)xfree, result);

      if (!len)
        gpgme_io_writen (fd, "\n", 1);
      else
        {
          int nl = result[len-1] == '\n';
          int ret;

          ret = gpgme_io_writen (fd, result, len);
          if (!ret && !nl)
            gpgme_io_writen (fd, "\n", 1);
          else if (ret)
            rc = GPG_ERR_CANCELED;
        }

      pthread_cleanup_pop (1);
    }

  return rc;
}

static void
progress_cb (void *data, const char *what, int type, int current, int total)
{
  struct crypto_s *crypto = data;

  crypto->progress_rc = send_status (crypto->client_ctx, STATUS_GPGME,
                                     "%s %i %i %i", what, type, current, total);
}

gpgme_error_t
crypto_data_to_buf (const gpgme_data_t data, unsigned char **result,
                    size_t *rlen)
{
  off_t ret = gpgme_data_seek (data, 0, SEEK_SET);
  gpgme_error_t rc;
  size_t total = 0, size = BUFSIZE;
  unsigned char *buf = NULL;

  *result = NULL;
  *rlen = 0;

  if (ret == -1)
    return gpg_error_from_syserror ();

  buf = xmalloc (size);
  if (!buf)
    return GPG_ERR_ENOMEM;

  do
    {
      ret = gpgme_data_read (data, &buf[total], BUFSIZE);
      if (ret > 0)
        {
          unsigned char *p;

          total += ret;
          size += BUFSIZE;
          p = xrealloc (buf, size * sizeof (unsigned char));
          if (!p)
            {
              xfree (buf);
              return GPG_ERR_ENOMEM;
            }

          buf = p;
        }
    } while (ret > 0);

  if (ret == -1)
    {
      rc = gpgme_err_code_from_syserror ();
      xfree (buf);
      return rc;
    }

  if (!total)
    xfree (buf);
  else
    *result = buf;

  *rlen = total;
  return 0;
}

gpgme_error_t
crypto_list_keys (struct crypto_s *crypto, char *keys[], int secret,
                  gpgme_key_t **result)
{
  gpgme_error_t rc;
  gpgme_key_t key = NULL, *res = NULL;
  size_t total = 0;

  rc = gpgme_op_keylist_ext_start (crypto->ctx, (const char **)keys, secret, 0);
  if (rc)
    return rc;

  STATUS_TIMEOUT_INIT (crypto);
  do
    {
      gpgme_key_t *p;

      pthread_cleanup_push ((void *)crypto_free_key_list, res);
      rc = gpgme_op_keylist_next (crypto->ctx, &key);
      pthread_cleanup_pop (0);
      if (rc && gpgme_err_code(rc) != GPG_ERR_EOF)
        break;

      if (rc)
        {
          rc = 0;
          break;
        }

      p = xrealloc (res, (total+2) * sizeof (gpgme_key_t));
      if (!p)
        {
          rc = GPG_ERR_ENOMEM;
          break;
        }

      res = p;
      res[total++] = key;
      res[total] = NULL;
      pthread_cleanup_push ((void *)crypto_free_key_list, res);
      STATUS_TIMEOUT_TEST (crypto, rc, STATUS_KEEPALIVE, NULL);
      pthread_cleanup_pop (0);
    } while (!rc);

  if (!rc)
    {
      rc = gpgme_op_keylist_end (crypto->ctx);
      if (!rc)
        {
          *result = res;
          rc = total ? 0 : secret ? GPG_ERR_NO_SECKEY : GPG_ERR_NO_PUBKEY;
        }
    }
  else
    crypto_free_key_list (res);

  return rc;
}

void
crypto_free_save (struct save_s *save)
{
  if (!save)
    return;

  strv_free (save->pubkey);
  xfree (save->sigkey);
  xfree (save->userid);
  xfree (save->algo);
  crypto_free_key_list (save->mainkey);

  memset (save, 0, sizeof (struct save_s));
}

static void
free_gpgme_data_cb (void *data)
{
  gpgme_data_t d = data;
  char *t;
  size_t len;

  if (!data)
    return;

  t = gpgme_data_release_and_get_mem (d, &len);
  if (t)
    wipememory (t, 0, len);

  gpgme_free (t);
}

void
crypto_free_non_keys (struct crypto_s *crypto)
{
  if (!crypto)
    return;

  if (crypto->ctx)
    gpgme_release (crypto->ctx);

  crypto->ctx = NULL;
  xfree (crypto->plaintext);
  crypto->plaintext = NULL;
  crypto->plaintext_size = 0;

  if (crypto->cipher)
    free_gpgme_data_cb (crypto->cipher);

  crypto->cipher = NULL;
  crypto_free_save (&crypto->save);
  xfree (crypto->keyfile);
  crypto->keyfile = NULL;
  crypto->flags &= ~CRYPTO_FLAG_KEYFILE;
}

void
crypto_free (struct crypto_s *crypto)
{
  if (!crypto)
    return;

  crypto_free_non_keys (crypto);
  strv_free (crypto->pubkey);
  xfree (crypto->sigkey);
  xfree (crypto->filename);
  xfree (crypto);
}

gpgme_error_t
crypto_init_ctx (struct crypto_s *crypto, int no_pinentry,
                 char *passphrase_file)
{
  gpgme_ctx_t ctx;
  gpgme_keylist_mode_t keylist_mode;
  gpgme_error_t rc;

  rc = gpgme_new (&ctx);
  if (rc)
    return rc;

  rc = gpgme_set_protocol (ctx, GPGME_PROTOCOL_OPENPGP);
  if (!rc)
    {
      keylist_mode = gpgme_get_keylist_mode (ctx);
      keylist_mode |= GPGME_KEYLIST_MODE_LOCAL|GPGME_KEYLIST_MODE_WITH_SECRET;
      rc = gpgme_set_keylist_mode (ctx, keylist_mode);
      if (!rc)
         {
           if (no_pinentry || passphrase_file)
             rc = gpgme_set_pinentry_mode (ctx, GPGME_PINENTRY_MODE_LOOPBACK);
           else
             rc = gpgme_set_pinentry_mode (ctx, GPGME_PINENTRY_MODE_DEFAULT);

           if (!rc)
             {
               gpgme_set_ctx_flag (ctx, "no-symkey-cache", "1");
               gpgme_set_passphrase_cb (ctx, passphrase_cb, crypto);
               gpgme_set_progress_cb (ctx, progress_cb, crypto);
               gpgme_set_status_cb (ctx, status_cb, crypto);
               crypto->ctx = ctx;
               crypto->flags = 0;

               if (passphrase_file)
                 {
                   crypto->keyfile = passphrase_file;
                   crypto->flags |= CRYPTO_FLAG_KEYFILE;
                 }
             }
         }
    }

  if (rc)
    gpgme_release (ctx);

  return rc;
}

void
crypto_set_keepalive ()
{
  keepalive = config_get_integer ("global", "keepalive_interval");
}

gpg_error_t
crypto_init (struct crypto_s **crypto, void *ctx, const char *filename,
             int no_pinentry, char *passphrase_file)
{
  struct crypto_s *new = xcalloc (1, sizeof (struct crypto_s));
  gpgme_error_t rc;

  crypto_set_keepalive ();

  if (!new)
    return GPG_ERR_ENOMEM;

  rc = crypto_init_ctx (new, no_pinentry, passphrase_file);
  if (rc)
    goto fail;

  if (filename)
    {
      new->filename = str_dup (filename);
      if (!new->filename)
        {
          rc = GPG_ERR_ENOMEM;
          goto fail;
        }
    }

  new->client_ctx = ctx;
  *crypto = new;
  return 0;

fail:
  crypto_free (new);
  return rc;
}

/* Converts a 40 byte key id to the 16 byte form. Needed for comparison of
 * gpgme_recipient_t.keyid. */
gpg_error_t
crypto_keyid_to_16b_once (char *key)
{
  size_t len = strlen (key);

  if (len < 16)
    return GPG_ERR_INV_ARG;

  memmove (&key[0], &key[len-16], 16);
  key[16] = 0;
  return 0;
}

gpg_error_t
crypto_keyid_to_16b (char **keys)
{
  char **p;

  for (p = keys; p && *p; p++)
    {
      gpg_error_t rc = crypto_keyid_to_16b_once (*p);
      if (rc)
        return rc;
    }

  return 0;
}

gpgme_error_t
crypto_decrypt (struct client_s *client, struct crypto_s *crypto)
{
  gpgme_data_t plain;
  gpgme_error_t rc;
  gpgme_data_t cipher;
  int fd = -1;
  struct stat st;
  char **new_recipients = NULL;
  char **new_signers = NULL;
  char **pp;
  gpgme_decrypt_result_t result;

  rc = gpgme_data_new (&plain);
  if (rc)
    return rc;

  pthread_cleanup_push ((void *)free_gpgme_data_cb, plain);
  rc = open_check_file (crypto->filename, &fd, &st, 1);
  if (!rc)
    {
      pthread_cleanup_push ((void *)close_fd_cb, &fd);
      rc = gpgme_data_new_from_fd (&cipher, fd);
      if (!rc)
        {
          pthread_cleanup_push ((void *)free_gpgme_data_cb, cipher);
          rc = send_status (client ? client->ctx : NULL, STATUS_DECRYPT, NULL);
          if (!rc)
            {
              MUTEX_TRYLOCK ((client ? client->ctx : NULL), &crypto_mutex, rc,
                             (client ? client->lock_timeout : -1));
              if (!rc)
                {
                  pthread_cleanup_push (release_mutex_cb, &crypto_mutex);
                  rc = gpgme_op_decrypt_verify_start (crypto->ctx, cipher, plain);
                  if (!rc)
                    {
                      STATUS_TIMEOUT_INIT(crypto);
                      do
                        {
                          if (!rc && crypto->progress_rc)
                            rc = crypto->progress_rc;
                          if (!rc)
                            STATUS_TIMEOUT_TEST (crypto, rc, STATUS_DECRYPT, NULL);
                          TEST_CANCEL ();
                        } while (!rc && !gpgme_wait (crypto->ctx, &rc, 0) && !rc);
                    }
                  pthread_cleanup_pop (1); // release_mutex_cb
                }
            }
          pthread_cleanup_pop (1); // free_gpgme_data_cb (cipher)
        }

      pthread_cleanup_pop (1); // close (fd)
    }

  pthread_cleanup_pop (0); // free_gpgme_data_cb (plain)

  if (rc)
    {
      free_gpgme_data_cb (plain);
      return rc;
    }

  pthread_cleanup_push ((void *)free_gpgme_data_cb, plain);
  result = gpgme_op_decrypt_result (crypto->ctx);
  if (!result->unsupported_algorithm && !result->wrong_key_usage)
    {
      gpgme_recipient_t r;

      for (r = result->recipients; r; r = r->next)
        {
          pthread_cleanup_push ((void *)strv_free, new_recipients);
          log_write1 (_ ("%s: recipient: %s, status=%u"),
                      crypto->filename, r->keyid, r->status);
          pthread_cleanup_pop (0);
          pp = strv_cat(new_recipients, str_dup (r->keyid));
          if (!pp)
            {
              rc = GPG_ERR_ENOMEM;
              break;
            }

          new_recipients = pp;
        }
    }
  else if (result->wrong_key_usage)
    rc = GPG_ERR_WRONG_KEY_USAGE;
  else if (result->unsupported_algorithm)
    rc = GPG_ERR_UNSUPPORTED_ALGORITHM;

  if (!rc)
    {
      gpgme_verify_result_t verify;
      gpgme_signature_t s;

      verify = gpgme_op_verify_result (crypto->ctx);
      for (s = verify->signatures; s; s = s->next)
        {
          unsigned flags = s->summary;

          pthread_cleanup_push ((void *)strv_free, new_signers);
          log_write1 (_ ("%s: signer: %s, status=%u"),
                      crypto->filename, s->fpr, s->status);
          pthread_cleanup_pop (0);

          flags &= ~(GPGME_SIGSUM_KEY_EXPIRED|GPGME_SIGSUM_SIG_EXPIRED);
          if (!flags)
            flags |= GPGME_SIGSUM_VALID;

          if (gpg_err_code (s->status) == GPG_ERR_KEY_EXPIRED)
            s->status = 0;

          if (s->status || s->wrong_key_usage || !(flags & GPGME_SIGSUM_VALID))
            continue;

          pp = strv_cat (new_signers, str_dup (s->fpr));
          if (!pp)
            {
              rc = GPG_ERR_ENOMEM;
              break;
            }

          new_signers = pp;
        }

      if (verify->signatures && !new_signers)
        rc = GPG_ERR_BAD_SIGNATURE;

      if (!rc)
        {
          xfree (crypto->plaintext);
          crypto->plaintext = NULL;
          crypto->plaintext_size = 0;
          pthread_cleanup_push ((void *)strv_free, new_recipients);
          pthread_cleanup_push ((void *)strv_free, new_signers);
          rc = crypto_data_to_buf (plain, &crypto->plaintext,
                                   &crypto->plaintext_size);
          pthread_cleanup_pop (0);
          pthread_cleanup_pop (0);
          if (!rc)
            {
              strv_free (crypto->pubkey);
              crypto->pubkey = new_recipients;
              crypto_keyid_to_16b (crypto->pubkey);

              xfree (crypto->sigkey);
              crypto->sigkey = NULL;
              if (new_signers)
                {
                  crypto->sigkey = str_dup (*new_signers);
                  strv_free (new_signers);
                  crypto_keyid_to_16b_once (crypto->sigkey);
                }
            }
        }
    }

  if (rc)
    {
      strv_free (new_recipients);
      strv_free (new_signers);
    }

  pthread_cleanup_pop (1); // free_gpgme_data_cb (plain)
  return rc;
}

void
crypto_free_key_list (gpgme_key_t *keys)
{
  int i;

  for (i = 0; keys && keys[i]; i++)
    gpgme_key_unref (keys[i]);

  xfree (keys);
}

/* Removes strings in 'prune' from 'a'. */
static char **
prune_keys (char **a, char **prune)
{
  char **p;

  for (p = prune; p && *p; p++)
    {
      char **ap;

      for (ap = a; ap && *ap; ap++)
        {
          if (!strcmp (*ap, *p))
            {
              while (*ap)
                {
                  *ap = *(ap+1);
                  ap++;
                }
            }
        }
    }

  return a;
}

static void
remove_duplicates (char **a)
{
  char **p;

  for (p = a; p && *p; p++)
    {
      char **t;

      for (t = p+1; t && *t; t++)
        {
          if (!strcmp (*p, *t))
            {
              char *tmp = *t;

              while (*(t+1))
                {
                  *t = *(t+1);
                  t++;
                }

              *t = NULL;
              xfree (tmp);
              remove_duplicates (a);
              return;
            }
        }
    }
}

gpgme_error_t
crypto_encrypt (struct client_s *client, struct crypto_s *crypto)
{
  gpgme_error_t rc;
  gpgme_data_t cipher = NULL;
  gpgme_key_t *keys = NULL;
  gpgme_key_t *sigkeys = NULL;
  unsigned flags = 0;
  int sign = 0;

  if (!((crypto->flags & CRYPTO_FLAG_SYMMETRIC)))
    {
      crypto_keyid_to_16b (crypto->save.pubkey);
      remove_duplicates (crypto->save.pubkey);
      rc = crypto_list_keys (crypto, crypto->save.pubkey, 0, &keys);
      if (rc)
        return rc;
    }

  pthread_cleanup_push ((void *)crypto_free_key_list, keys);
  rc = gpgme_data_new (&cipher);
  pthread_cleanup_push ((void *)free_gpgme_data_cb, cipher);
  if (!rc)
    rc = gpgme_data_set_file_name (cipher, crypto->filename);

  if (!rc && crypto->save.sigkey)
    {
      char **tmp = NULL;

      crypto_keyid_to_16b_once (crypto->save.sigkey);
      strv_printf (&tmp, "%s", crypto->save.sigkey);
      pthread_cleanup_push ((void *)strv_free, tmp);
      rc = crypto_list_keys (crypto, tmp, 1, &sigkeys);
      pthread_cleanup_pop (1);
      if (gpg_err_code (rc) == GPG_ERR_NO_DATA)
        rc = 0;
    }

  if (!rc)
    {
      gpgme_data_t plain = NULL;

      pthread_cleanup_push ((void *)crypto_free_key_list, sigkeys);
      rc = gpgme_data_new_from_mem (&plain, (char *)crypto->plaintext,
                                    crypto->plaintext_size, 0);
      pthread_cleanup_push ((void *)free_gpgme_data_cb, plain);
      if (!rc)
        {
          int i;

          gpgme_signers_clear (crypto->ctx);

          for (i = 0; sigkeys && sigkeys[i]; i++)
            gpgme_signers_add (crypto->ctx, sigkeys[i]);
        }

      if (!rc)
        {
          gpgme_data_set_encoding (cipher, GPGME_DATA_ENCODING_ARMOR);
          rc = send_status (client ? client->ctx : NULL, STATUS_ENCRYPT, NULL);
        }

      if (!rc)
        {
          int f = config_get_boolean ("global", "encrypt_to");

          if (!f)
            flags |= GPGME_ENCRYPT_NO_ENCRYPT_TO;

          f = config_get_boolean ("global", "always_trust");
          if (f)
            flags |= GPGME_ENCRYPT_ALWAYS_TRUST;

          if (!(crypto->flags & CRYPTO_FLAG_SYMMETRIC)
              || (crypto->flags & CRYPTO_FLAG_SYMMETRIC && sigkeys))
            sign = 1;
        }

      if (!rc)
        MUTEX_TRYLOCK ((client ? client->ctx : NULL), &crypto_mutex, rc,
                       (client ? client->lock_timeout : -1));

      if (!rc)
        {
          pthread_cleanup_push (release_mutex_cb, &crypto_mutex);
          if (sign)
            rc = gpgme_op_encrypt_sign_start (crypto->ctx, keys, flags, plain,
                                              cipher);
          else
            rc = gpgme_op_encrypt_start (crypto->ctx, NULL, flags, plain,
                                         cipher);
          if (!rc)
            {
              STATUS_TIMEOUT_INIT (crypto);
              do
                {
                  if (!rc && crypto->progress_rc)
                    rc = crypto->progress_rc;
                  if (!rc)
                    STATUS_TIMEOUT_TEST (crypto, rc, STATUS_ENCRYPT, NULL);
                  TEST_CANCEL();
                } while (!rc && !gpgme_wait (crypto->ctx, &rc, 0) && !rc);
            }
          pthread_cleanup_pop (1);

          if (!rc)
            {
              gpgme_encrypt_result_t result;
              gpgme_sign_result_t sresult;
              gpgme_invalid_key_t inv;
              gpgme_new_signature_t sigs;
              char **prune = NULL;
              char **p = NULL;

              result = gpgme_op_encrypt_result (crypto->ctx);
              inv = result->invalid_recipients;
              while (inv)
                {
                  pthread_cleanup_push ((void *)strv_free, prune);
                  log_write1 (_ ("%s: invalid recipient: %s, rc=%u"),
                              crypto->filename, inv->fpr, inv->reason);
                  pthread_cleanup_pop (0);
                  p = strv_cat (prune, str_dup(inv->fpr));
                  if (!p)
                    {
                      rc = GPG_ERR_ENOMEM;
                      strv_free (prune);
                      break;
                    }

                  prune = p;
                  inv = inv->next;
                }

              if (prune)
                {
                  p = prune_keys (crypto->save.pubkey, prune);
                  crypto->save.pubkey = p;
                  strv_free (prune);
                }

              crypto_keyid_to_16b (crypto->save.pubkey);
              sresult = gpgme_op_sign_result (crypto->ctx);
              inv = sresult ? sresult->invalid_signers : NULL;
              while (!rc && inv)
                {
                  log_write1 (_ ("%s: invalid signer: %s, rc=%u"),
                              crypto->filename, inv->fpr, inv->reason);
                  inv = inv->next;
                }

              sigs = sresult ? sresult->signatures : NULL;
              if (!rc && sigs)
                {
                  p = NULL;

                  while (sigs)
                    {
                      char **pp;

                      pthread_cleanup_push ((void *)strv_free, p);
                      log_write1 (_ ("%s: signer: %s"), crypto->filename,
                                  sigs->fpr);
                      pthread_cleanup_pop (0);

                      pp = strv_cat (p, str_dup (sigs->fpr));
                      if (!pp)
                        {
                          rc = GPG_ERR_ENOMEM;
                          strv_free (p);
                          break;
                        }

                      p = pp;
                      sigs = sigs->next;
                    }

                  if (!rc)
                    {
                      xfree (crypto->save.sigkey);
                      crypto->save.sigkey = str_dup (*p);
                      strv_free (p);
                      crypto_keyid_to_16b_once (crypto->save.sigkey);
                      crypto->cipher = cipher;
                    }
                }
              else if (!rc && crypto->flags & CRYPTO_FLAG_SYMMETRIC)
                {
                  crypto->cipher = cipher;
                }
              else if (!rc)
                {
                 if (!(crypto->flags & CRYPTO_FLAG_SYMMETRIC)
                     || (crypto->flags & CRYPTO_FLAG_SYMMETRIC && sigkeys))
                   rc = GPG_ERR_NO_SIGNATURE_SCHEME;
                }
            }
        }

      pthread_cleanup_pop (1); // free_gpgme_data_cb (plain)
      pthread_cleanup_pop (1); // crypto_free_key_list (sigkeys)
    }

  pthread_cleanup_pop (0); // free_gpgme_data_cb (cipher)
  pthread_cleanup_pop (1); // crypto_free_key_list (keys)

  if (rc)
    free_gpgme_data_cb (cipher);

  return rc;
}

gpgme_error_t
crypto_passwd (struct client_s *client, struct crypto_s *crypto)
{
  gpgme_error_t rc;
  gpgme_key_t *keys = NULL;

  /* Use SAVE instead for symmetric files. */
  rc = crypto_is_symmetric (client->filename);
  if (!rc || rc != GPG_ERR_BAD_DATA)
    return !rc ? GPG_ERR_NOT_SUPPORTED : rc;

  /* Cannot change the passphrase of a key stored on a smartcard. Use
   * gpg --card-edit instead. */
  rc = cache_is_shadowed (client->filename);
  if (!rc)
    return GPG_ERR_NOT_SUPPORTED;
  else if (rc != GPG_ERR_NO_DATA)
    return rc;

  rc = crypto_list_keys (crypto, crypto->pubkey, 1, &keys);
  if (rc)
    return rc;

  crypto->flags |= CRYPTO_FLAG_PASSWD;
  pthread_cleanup_push ((void *)crypto_free_key_list, keys);
  rc = send_status (client->ctx, STATUS_KEEPALIVE, NULL);
  if (!rc)
    MUTEX_TRYLOCK (client->ctx, &crypto_mutex, rc, client->lock_timeout);

  if (!rc)
    {
      pthread_cleanup_push (release_mutex_cb, &crypto_mutex);
      rc = gpgme_op_passwd_start (crypto->ctx, keys[0], 0);
      if (!rc)
        {
          STATUS_TIMEOUT_INIT (crypto);
          do
            {
              if (!rc && crypto->progress_rc)
                rc = crypto->progress_rc;
              if (!rc)
                STATUS_TIMEOUT_TEST (crypto, rc, STATUS_KEEPALIVE, NULL);
              TEST_CANCEL();
            } while (!rc && !gpgme_wait (crypto->ctx, &rc, 0) && !rc);
        }
      pthread_cleanup_pop (1);
    }

  pthread_cleanup_pop (1);
  return rc;
}

/* Generate a new key pair. The resulting keyid's are stored in crypto->save.
 */
gpgme_error_t
crypto_genkey (struct client_s *client, struct crypto_s *crypto)
{
  gpgme_error_t rc;

  rc = send_status (client ? client->ctx : NULL, STATUS_GENKEY, NULL);
  if (!rc)
    MUTEX_TRYLOCK ((client ? client->ctx : NULL), &crypto_mutex, rc,
                   (client ? client->lock_timeout : 0));

  if (!rc)
    {
      pthread_cleanup_push (release_mutex_cb, &crypto_mutex);
      if (crypto->save.mainkey)
        {
          rc = gpgme_op_createsubkey_start (crypto->ctx,
                                            crypto->save.mainkey[0],
                                            crypto->save.algo,
                                            0,
                                            crypto->save.expire,
                                            crypto->save.flags);
        }
      else
        {
          rc = gpgme_op_createkey_start (crypto->ctx,
                                         crypto->save.userid,
                                         crypto->save.algo,
                                         0,
                                         crypto->save.expire,
                                         NULL,
                                         crypto->save.flags);
        }

      if (!rc)
        {
          STATUS_TIMEOUT_INIT (crypto);
          do
            {
              if (!rc && crypto->progress_rc)
                rc = crypto->progress_rc;
              if (!rc)
                STATUS_TIMEOUT_TEST (crypto, rc, STATUS_GENKEY, NULL);
              TEST_CANCEL();
            } while (!rc && !gpgme_wait (crypto->ctx, &rc, 0) && !rc);
        }
      pthread_cleanup_pop (1);
    }

  if (!rc)
    {
      gpgme_key_t *keys = NULL;
      gpgme_genkey_result_t result;

      result = gpgme_op_genkey_result (crypto->ctx);
      xfree (crypto->save.sigkey);
      crypto->save.sigkey = str_dup (result->fpr);
      crypto_keyid_to_16b_once (crypto->save.sigkey);

      if (!rc)
        {
          char **tmp = NULL;

          strv_printf (&tmp, "%s", crypto->save.sigkey);
          pthread_cleanup_push ((void *)strv_free, tmp);
          rc = crypto_list_keys (crypto, tmp, 1, &keys);
          pthread_cleanup_pop (1);
        }

      if (!rc)
        {
          gpgme_subkey_t key = keys[0]->subkeys;

          for (; key; key = key->next)
            {
              if (key->can_encrypt)
                break;
            }

          pthread_cleanup_push ((void *)crypto_free_key_list, keys);
          rc = send_status (client ? client->ctx : NULL, STATUS_GENKEY, "%s %s",
                            crypto->save.sigkey, key ? key->fpr : "");
          if (!rc && key)
            {
              crypto->save.pubkey = strv_cat (crypto->save.pubkey,
                                              str_dup (key->fpr));
              crypto_keyid_to_16b (crypto->save.pubkey);
            }

          pthread_cleanup_pop (1);
        }
    }

  return rc;
}

#ifdef WITH_LIBACL
static void
acl_free_cb (void *arg)
{
  acl_t acl = arg ? (acl_t) arg : NULL;

  if (acl)
    acl_free (acl);
}
#endif

/* The advisory lock should be obtained before calling this function. */
gpg_error_t
crypto_write_file (struct crypto_s *crypto, unsigned char **r_crc,
                   size_t *r_crclen)
{
  unsigned char *buf;
  size_t size, len;
  int fd;
  gpg_error_t rc;
  char tmp[PATH_MAX];
  struct stat st;
  mode_t mode = 0600;
#ifdef WITH_LIBACL
  acl_t acl = NULL;
#endif

  if (crypto->filename)
    {
      if (lstat (crypto->filename, &st) == 0)
	{
	  mode = st.st_mode & (S_IRWXU | S_IRWXG | S_IRWXO);

	  if (!(mode & S_IWUSR))
	    return GPG_ERR_EACCES;
	}
      else if (errno != ENOENT)
	return gpg_error_from_errno (errno);

      snprintf (tmp, sizeof (tmp), "%s.XXXXXX", crypto->filename);
      mode_t tmode = umask (0600);
      fd = mkstemp (tmp);
      if (fd == -1)
	{
	  rc = gpg_error_from_errno (errno);
          umask (tmode);
	  log_write ("%s: %s", tmp, pwmd_strerror (rc));
	  return rc;
	}

      umask (tmode);
    }
  else
    fd = STDOUT_FILENO;

  rc = crypto_data_to_buf (crypto->cipher, &buf, &size);
  if (rc)
    {
      if (crypto->filename)
        close (fd);
      return rc;
    }

  pthread_cleanup_push ((void *)close_fd_cb, &fd);
  if (r_crc)
    rc = get_checksum_memory (buf, size, r_crc, r_crclen);

  if (!rc)
    {
      pthread_cleanup_push ((void *)xfree, buf);
      len = write (fd, buf, size);
      pthread_cleanup_pop (1);
      if (len != size)
        rc = gpg_error_from_errno (errno);
    }

  if (!rc)
    {
      if (fsync (fd) != -1)
	{
	  if (crypto->filename && close (fd) != -1)
	    {
#ifdef WITH_LIBACL
              acl = acl_get_file (crypto->filename, ACL_TYPE_ACCESS);
              if (!acl && errno == ENOENT)
                acl = acl_get_file (".", ACL_TYPE_DEFAULT);
              if (!acl)
                log_write ("ACL: %s: %s", crypto->filename,
                           pwmd_strerror (gpg_error_from_errno (errno)));
              pthread_cleanup_push ((void *)acl_free_cb, acl);
#endif

              fd = -1;
	      if (config_get_boolean (crypto->filename, "backup"))
		{
		  char tmp2[PATH_MAX];

		  snprintf (tmp2, sizeof (tmp2), "%s.backup", crypto->filename);
		  if (rename (crypto->filename, tmp2) == -1)
                    if (errno != ENOENT)
                      rc = gpg_error_from_errno (errno);
		}

              if (!rc && rename (tmp, crypto->filename) == -1)
                rc = gpg_error_from_errno (errno);

              if (!rc)
                {
                  if (chmod (crypto->filename, mode) == -1)
                    log_write ("%s(%u): %s", __FILE__, __LINE__,
                               pwmd_strerror (gpg_error_from_syserror ()));
                }
#ifdef WITH_LIBACL
              if (!rc && acl && acl_set_file (crypto->filename,
                                              ACL_TYPE_ACCESS, acl))
                log_write ("ACL: %s: %s", crypto->filename,
                           pwmd_strerror (gpg_error_from_errno (errno)));
              pthread_cleanup_pop (1);
#endif
	    }
	  else if (crypto->filename)
	    rc = gpg_error_from_errno (errno);

          if (!rc && fd != -1)
            {
              char *datadir = str_asprintf ("%s/data", homedir);

              if (datadir)
                {
                  int dfd = open (datadir, O_RDONLY);

                  if (dfd != -1)
                    {
                      if (fsync (dfd) == -1)
                        log_write ("%s %d: %s", __FILE__, __LINE__,
                                   pwmd_strerror (errno));

                      close (dfd);
                    }
                  else
                    log_write ("%s %d: %s", __FILE__, __LINE__,
                               pwmd_strerror (errno));

                  xfree (datadir);
                }
            }
	}
      else
	rc = gpg_error_from_errno (errno);
    }

  pthread_cleanup_pop (0); // close (fd)
  if (fd != -1)
    close (fd);

  return rc;
}

char *
crypto_key_info (const gpgme_key_t key)
{
  struct string_s *string = string_new (NULL), *s;
  char *line = NULL;
  unsigned n, u;
  gpgme_subkey_t subkey;
  gpgme_user_id_t uid;

  if (!string)
    return NULL;

  for (u = 0, uid = key->uids; uid; uid = uid->next)
    u++;

  for (n = 0, subkey = key->subkeys; subkey; subkey = subkey->next)
    n++;

  s = string_append_printf (string, "%u:%u:%u:%u:%u:%u:%u:%u:%u:%u:%i:%s:%s:%s:%i:%u:%u",
                            key->revoked, key->expired, key->disabled,
                            key->invalid, key->can_encrypt, key->can_sign,
                            key->can_certify, key->secret,
                            key->can_authenticate, key->is_qualified,
                            key->protocol,
                            key->issuer_serial ? key->issuer_serial : "",
                            key->issuer_name ? key->issuer_name : "",
                            key->chain_id ? key->chain_id : "",
                            key->owner_trust, u, n);
  if (!s)
    {
      string_free (string, 1);
      return NULL;
    }

  string = s;
  for (subkey = key->subkeys; subkey; subkey = subkey->next)
    {
      char *tmp;

      s = string_append_printf (string, ":%u:%u:%u:%u:%u:%u:%u:%u:%u:%u:%u:%i:%u",
                                subkey->revoked, subkey->expired,
                                subkey->disabled, subkey->invalid,
                                subkey->can_encrypt, subkey->can_sign,
                                subkey->can_certify, subkey->secret,
                                subkey->can_authenticate, subkey->is_qualified,
                                subkey->is_cardkey, subkey->pubkey_algo,
                                subkey->length);
      if (!s)
        {
          string_free (string, 1);
          return NULL;
        }

      string = s;
      s = string_append_printf (string, ":%s:%s:%s:%li:%li:%s",
                                subkey->keyid, subkey->fpr,
                                subkey->keygrip ? subkey->keygrip : "",
                                subkey->timestamp, subkey->expires,
                                subkey->card_number ? subkey->card_number : "0");
      if (!s)
        {
          string_free (string, 1);
          return NULL;
        }

      string = s;
      tmp = gnupg_escape (subkey->curve);
      s = string_append_printf (string, ":%s", tmp ? tmp : "");
      xfree (tmp);
      if (!s)
        {
          string_free (string, 1);
          return NULL;
        }

      string = s;
    }

  for (uid = key->uids; uid; uid = uid->next)
    {
      char *userid, *name, *email, *comment;

      userid = gnupg_escape (uid->uid);
      name = gnupg_escape (uid->name);
      email = gnupg_escape (uid->email);
      comment = gnupg_escape (uid->comment);
      s = string_append_printf (string, ":%u:%u:%u:%s:%s:%s:%s",
                                uid->revoked, uid->invalid, uid->validity,
                                userid ? userid : "",
                                name ? name : "",
                                email ? email : "",
                                comment ? comment : "");
      xfree (userid);
      xfree (name);
      xfree (email);
      xfree (comment);
      if (!s)
        {
          string_free (string, 1);
          return NULL;
        }

      string = s;
    }

  line = string->str;
  string_free (string, 0);
  return line;
}

gpg_error_t
crypto_try_decrypt (struct client_s *client, int no_pinentry)
{
  struct crypto_s *crypto;
  gpg_error_t rc;
  char *keyfile = config_get_string (client->filename, "passphrase_file");

  rc = crypto_init (&crypto, client->ctx, client->filename, no_pinentry,
                    keyfile);
  if (!rc)
    {
      pthread_cleanup_push ((void *)crypto_free, crypto);
      rc = crypto_decrypt (client, crypto);
      pthread_cleanup_pop (1);
    }
  else
    xfree (keyfile);

  return rc;
}

/* The advisory lock should be obtained before calling this function. */
gpg_error_t
crypto_is_symmetric (const char *filename)
{
  int fd;
  uint8_t magic[2];
  ssize_t len;
  gpg_error_t rc;

  rc = open_check_file (filename, &fd, NULL, 1);
  if (rc)
    return rc;

  len = read (fd, &magic, sizeof(magic));
  close (fd);
  if (len != sizeof (magic))
    return GPG_ERR_INV_VALUE;

  // Always read as big endian.
  if (magic[0] != 0x8c || magic[1] != 0x0d)
    return GPG_ERR_BAD_DATA;

  return 0;
}

gpg_error_t
crypto_delete_key (struct client_s *client, struct crypto_s *crypto,
                   const gpgme_key_t key, int secret)
{
  gpg_error_t rc;

  STATUS_TIMEOUT_INIT (crypto);
  rc = send_status (client->ctx, STATUS_DECRYPT, NULL);
  if (!rc)
    rc = gpgme_op_delete_ext_start (crypto->ctx, key, GPGME_DELETE_FORCE
                                    | GPGME_DELETE_ALLOW_SECRET);

  if (rc)
    return rc;

  do
    {
      if (!rc && crypto->progress_rc)
        rc = crypto->progress_rc;

      if (!rc)
        STATUS_TIMEOUT_TEST (crypto, rc, STATUS_DECRYPT, NULL);

      if (rc)
        break;
    } while (!gpgme_wait (crypto->ctx, &rc, 0) && !rc);

  return rc;
}
