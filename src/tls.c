/*
    Copyright (C) 2006-2021 Ben Kibbey <bjk@luxsci.net>

    This file is part of pwmd.

    Pwmd is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    Pwmd is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Pwmd.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gnutls/x509.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>

#ifdef HAVE_STRINGS_H
#include <strings.h>
#endif

#include "pwmd-error.h"
#include <gcrypt.h>
#include "mem.h"
#include "util-misc.h"
#include "util-string.h"
#include "common.h"
#include "tls.h"
#include "mutex.h"
#include "rcfile.h"

#define WAIT_INTERVAL 50000
#define TEST_TIMEOUT(timeout, ret, start, rc)				\
  do {									\
    rc = 0;		      						\
    if (ret == GNUTLS_E_AGAIN)						\
      {									\
	time_t now = time (NULL);					\
	if (timeout && now - start >= timeout)				\
	  {								\
	    rc = gpg_error (GPG_ERR_ETIMEDOUT);				\
            ret = GNUTLS_E_TIMEDOUT;					\
	    break;							\
	  }								\
        struct timeval wtv = { 0, WAIT_INTERVAL };			\
        select (0, NULL, NULL, NULL, &wtv);				\
      }									\
    if (ret != GNUTLS_E_INTERRUPTED)					\
      break;								\
  }									\
  while (0)

static gnutls_dh_params_t dh_params;
static gnutls_certificate_credentials_t x509_cred;
int tls_fd;
int tls6_fd;

static char *
tls_fingerprint (gnutls_session_t ses)
{
  gnutls_x509_crt_t crt;
  const gnutls_datum_t *cert_list;
  unsigned count;
  unsigned char buf[32];
  size_t len = sizeof (buf);

  gnutls_x509_crt_init (&crt);
  if (!crt)
    {
      log_write ("%s(%i): %s(): %s", __FILE__, __LINE__, __FUNCTION__,
		 gnutls_strerror (GNUTLS_E_MEMORY_ERROR));
      return NULL;
    }

  cert_list = gnutls_certificate_get_peers (ses, &count);
  if (count)
    {
      gnutls_x509_crt_import (crt, &cert_list[0], GNUTLS_X509_FMT_DER);
      gnutls_x509_crt_get_fingerprint (crt, GNUTLS_DIG_SHA256, buf, &len);
      gnutls_x509_crt_deinit (crt);

      return len ? bin2hex (buf, len) : NULL;
    }

  gnutls_x509_crt_deinit (crt);
  return NULL;
}

static int
verify_client_certificate (unsigned status)
{
  if (!status)
    return 0;

  if (status & GNUTLS_CERT_REVOKED)
    log_write (_("client certificate is revoked"));

  if (status & GNUTLS_CERT_SIGNER_NOT_FOUND)
    log_write (_("client certificate has no signer"));

  if (status & GNUTLS_CERT_SIGNATURE_FAILURE)
    log_write (_("client certificate signature verification failed"));

  if (status & GNUTLS_CERT_EXPIRED)
    log_write (_("client certificate expired"));

  if (status & GNUTLS_CERT_SIGNER_NOT_CA)
    log_write (_("client certificate signer is not from CA"));

  if (status & GNUTLS_CERT_INSECURE_ALGORITHM)
    log_write (_("client certificate has insecure algorithm"));

  if (status & GNUTLS_CERT_INVALID)
    log_write (_("client certificate is invalid"));

  return GNUTLS_E_CERTIFICATE_ERROR;
}

struct tls_s *
tls_init_client (int fd, int timeout, const char *prio)
{
  int ret;
  unsigned status;
  const char *prio_error;
  gnutls_kx_algorithm_t kx;
  gpg_error_t rc = 0;
  struct tls_s *tls = xcalloc (1, sizeof (struct tls_s));

  if (!tls)
    {
      log_write ("%s(%i): %s: %s", __FILE__, __LINE__, __FUNCTION__,
		 strerror (ENOMEM));
      return NULL;
    }

  ret = gnutls_init (&tls->ses, GNUTLS_SERVER);
  if (ret != GNUTLS_E_SUCCESS)
    goto fail;

  ret = gnutls_priority_set_direct (tls->ses, prio, &prio_error);
  if (ret != GNUTLS_E_SUCCESS)
    goto fail;

  ret = gnutls_credentials_set (tls->ses, GNUTLS_CRD_CERTIFICATE, x509_cred);
  if (ret != GNUTLS_E_SUCCESS)
    goto fail;

  gnutls_certificate_server_set_request (tls->ses, GNUTLS_CERT_REQUIRE);
  gnutls_transport_set_ptr (tls->ses, (gnutls_transport_ptr_t) fd);
  time_t start = time (NULL);
  do
    {
      ret = gnutls_handshake (tls->ses);
      TEST_TIMEOUT (timeout, ret, start, rc);
      if (rc)
	goto fail;
    }
  while (ret == GNUTLS_E_AGAIN || ret == GNUTLS_E_INTERRUPTED);

  if (ret != GNUTLS_E_SUCCESS)
    goto fail;

  ret = gnutls_certificate_verify_peers2 (tls->ses, &status);
  if (ret)
    goto fail;

  kx = gnutls_kx_get (tls->ses);
  tls->fp = tls_fingerprint (tls->ses);
  log_write ("PROTO=%s CIPHER=%s MAC=%s KX=%s(%d) FP=%s",
	     gnutls_protocol_get_name (gnutls_protocol_get_version
				       (tls->ses)),
	     gnutls_cipher_get_name (gnutls_cipher_get (tls->ses)),
             gnutls_mac_get_name (gnutls_mac_get (tls->ses)),
	     gnutls_kx_get_name (kx), gnutls_dh_get_prime_bits (tls->ses),
	     tls->fp ? tls->fp : "N/A");
  ret = verify_client_certificate (status);
  if (ret)
    goto fail;

  return tls;

fail:
  log_write ("%s", rc ? pwmd_strerror(rc) : gnutls_strerror (ret));
  gnutls_deinit (tls->ses);
  xfree (tls->fp);
  xfree (tls);
  return NULL;
}

void
tls_log (int level, const char *msg)
{
  log_write ("TLS: %i: %s", level, msg);
}

void
tls_audit_log (gnutls_session_t s, const char *msg)
{
  (void)s;
  log_write ("TLS: %s", msg);
}

ssize_t
tls_read_hook (assuan_context_t ctx, assuan_fd_t fd, void *data, size_t len)
{
  struct client_s *client = assuan_get_pointer (ctx);
  struct tls_s *tls = client->thd->tls;
  time_t start = time (NULL);
  ssize_t ret;

  do
    {
      gpg_error_t rc = 0;
      struct timeval tv = { 0, WAIT_INTERVAL};
      fd_set fds;
      int n;

      FD_ZERO (&fds);
      FD_SET (fd, &fds);
      n = select (client->thd->fd+1, &fds, NULL, NULL, &tv);
      if (n == 0 && tls->nl)
        {
          char c[] = "#\n";

          memcpy (data, c, sizeof(c));
          ret = strlen (c);
          break;
        }

      ret = gnutls_record_recv (tls->ses, data, len);
      if (ret == GNUTLS_E_REHANDSHAKE)
	{
          tls->rehandshake = 0;

	  do
	    {
	      ret = gnutls_handshake (tls->ses);
	      TEST_TIMEOUT (client->thd->timeout, ret, start, rc);
	      if (rc)
                break;
	    }
	  while (ret < 0 && gnutls_error_is_fatal (ret) == 0);

          if (!ret)
            {
              gnutls_kx_algorithm_t kx;
              char c[] = "# \n";

              if (tls->nl)
                {
                  memcpy (data, c, sizeof (c));
                  ret = strlen (c);
                }
              else
                ret = GNUTLS_E_AGAIN;

              kx = gnutls_kx_get (tls->ses);
              log_write ("PROTO=%s CIPHER=%s MAC=%s KX=%s(%d)",
                         gnutls_protocol_get_name (gnutls_protocol_get_version
                                                   (tls->ses)),
                         gnutls_cipher_get_name (gnutls_cipher_get (tls->ses)),
                         gnutls_mac_get_name (gnutls_mac_get (tls->ses)),
                         gnutls_kx_get_name (kx), gnutls_dh_get_prime_bits (tls->ses));
              if (!tls->nl)
                continue;
            }
          break;
	}

      TEST_TIMEOUT (client->thd->timeout, ret, start, rc);
      if (rc)
        break;
    }
  while (ret == GNUTLS_E_INTERRUPTED || ret == GNUTLS_E_AGAIN);

  if (ret < 0)
    {
      log_write ("TLS: %s", gnutls_strerror (ret));
      ret = 0;
    }
  else if (ret > 0)
    tls->nl = ((char *)data)[ret-1] == '\n';

  return ret;
}

ssize_t
tls_write_hook (assuan_context_t ctx, assuan_fd_t fd, const void *data,
		size_t len)
{
  struct client_s *client = assuan_get_pointer (ctx);
  ssize_t ret;
  time_t start = time (NULL);

  if (client->disco)
    return -1;

  (void)fd;
  do
    {
      gpg_error_t rc = 0;

      ret = gnutls_record_send (client->thd->tls->ses, data, len);
      TEST_TIMEOUT (client->thd->timeout, ret, start, rc);
      if (rc)
        break;
    }
  while (ret == GNUTLS_E_INTERRUPTED || ret == GNUTLS_E_AGAIN);

  if (ret < 0)
    log_write ("TLS: %s", gnutls_strerror (ret));

  return ret;
}

static gpg_error_t
tls_init_params ()
{
  int n;
  char *tmp, *server_cert = NULL, *server_key = NULL;
  gpg_error_t rc = GPG_ERR_GENERAL;
  static pthread_mutex_t tls_mutex = PTHREAD_MUTEX_INITIALIZER;

  if (x509_cred)
    return 0;

  MUTEX_LOCK(&tls_mutex);
  pthread_cleanup_push (release_mutex_cb, &tls_mutex);
  MUTEX_LOCK (&rcfile_mutex);
  pthread_cleanup_push (release_mutex_cb, &rcfile_mutex);
  tls_deinit_params ();
  n = gnutls_certificate_allocate_credentials (&x509_cred);
  if (n != GNUTLS_E_SUCCESS)
    {
      log_write ("%s", gnutls_strerror (n));
      x509_cred = NULL;
      goto fail;
    }

  if (config_get_boolean ("global", "tls_use_crl"))
    {
      tmp = config_get_string ("global", "tls_crl_file");
      if (!tmp)
        tmp = str_asprintf ("%s/crl.pem", homedir);
      else
        {
          char *tmp2 = expand_homedir (tmp);
          xfree (tmp);
          tmp = tmp2;
        }

      if (!tmp)
	{
	  rc = GPG_ERR_ENOMEM;
	  goto fail;
	}

      if (access (tmp, R_OK) == -1 && errno == ENOENT)
        {
          rc = gpg_error_from_errno (errno);
          log_write ("%s: %s", tmp, pwmd_strerror (rc));
          goto fail;
        }
      else
	{
	  n = gnutls_certificate_set_x509_crl_file (x509_cred, tmp,
						    GNUTLS_X509_FMT_PEM);
	  if (n < 0)
	    {
	      log_write ("%s: %s", tmp, gnutls_strerror (n));
	      xfree (tmp);
	      goto fail;
	    }
	}

      xfree (tmp);
    }

  tmp = config_get_string ("global", "tls_ca_file");
  if (!tmp)
    tmp = str_asprintf ("%s/ca-cert.pem", homedir);
  else
    {
      char *tmp2 = expand_homedir (tmp);
      xfree (tmp);
      tmp = tmp2;
    }

  if (!tmp)
    {
      rc = GPG_ERR_ENOMEM;
      log_write ("%s(%i): %s", __FILE__, __LINE__, pwmd_strerror (rc));
      goto fail;
    }

  n = gnutls_certificate_set_x509_trust_file (x509_cred, tmp,
					      GNUTLS_X509_FMT_PEM);
  if (n < 0)
    {
      log_write ("%s: %s", tmp, gnutls_strerror (n));
      xfree (tmp);
      goto fail;
    }

  xfree (tmp);
  server_cert = config_get_string ("global", "tls_server_cert_file");
  if (!server_cert)
    server_cert = str_asprintf ("%s/server-cert.pem", homedir);
  else
    {
      tmp = expand_homedir (server_cert);
      xfree (server_cert);
      server_cert = tmp;
    }

  if (!server_cert)
    {
      rc = GPG_ERR_ENOMEM;
      log_write ("%s(%i): %s", __FILE__, __LINE__, pwmd_strerror (rc));
      goto fail;
    }

  server_key = config_get_string ("global", "tls_server_key_file");
  if (!server_key)
    server_key = str_asprintf ("%s/server-key.pem", homedir);
  else
    {
      tmp = expand_homedir (server_key);
      xfree (server_key);
      server_key = tmp;
    }

  if (!server_key)
    {
      xfree (server_cert);
      rc = GPG_ERR_ENOMEM;
      log_write ("%s(%i): %s", __FILE__, __LINE__, pwmd_strerror (rc));
      goto fail;
    }

  n = gnutls_certificate_set_x509_key_file (x509_cred, server_cert, server_key,
					    GNUTLS_X509_FMT_PEM);
  xfree (server_cert);
  xfree (server_key);
  if (n != GNUTLS_E_SUCCESS)
    {
      log_write ("%s", gnutls_strerror (n));
      goto fail;
    }

  tmp = config_get_string ("global", "tls_dh_params_file");
  if (tmp)
    {
      char *tmp2 = expand_homedir (tmp);
      if (!tmp2)
        {
          log_write ("%s: %s", tmp, pwmd_strerror (GPG_ERR_ENOMEM));
          xfree (tmp);
          rc = GPG_ERR_ENOMEM;
          goto fail;
        }

      xfree (tmp);
      tmp = tmp2;
      n = gnutls_dh_params_init (&dh_params);
      if (!n)
        {
          gnutls_datum_t data;

          n = gnutls_load_file (tmp, &data);
          if (!n)
            {
              n = gnutls_dh_params_import_pkcs3 (dh_params, &data,
                                                 GNUTLS_X509_FMT_PEM);
              if (!n)
                gnutls_certificate_set_dh_params (x509_cred, dh_params);

              wipememory (data.data, 0, data.size);
              free (data.data);
            }
        }

      if (n != GNUTLS_E_SUCCESS)
        {
          log_write ("%s: %s", tmp, gnutls_strerror (n));
          xfree (tmp);
          goto fail;
        }
      xfree (tmp);
    }

  rc = 0;

fail:
  if (rc)
    tls_deinit_params ();

  pthread_cleanup_pop (1); // tls_mutex
  pthread_cleanup_pop (1); // rcfile_mutex
  return rc;
}

void
tls_deinit_params ()
{
  if (dh_params)
    {
      gnutls_dh_params_deinit (dh_params);
      dh_params = NULL;
    }

  if (x509_cred)
    {
      gnutls_certificate_free_credentials (x509_cred);
      x509_cred = NULL;
    }
}

static gpg_error_t
do_tls_validate_access (struct client_s *client, const char *section)
{
  char **access = config_get_list (section, "allowed");
  char **p;
  int allowed = 0;

  if (!access || !*access)
    return GPG_ERR_EACCES;

  for (p = access; p && *p; p++)
    {
      int not = 0;
      char *fp = *p;

      if (*fp && *fp == '+' && *(fp + 1) == 0) // allow all connections
	{
	  allowed = 1;
	  continue;
	}

      if (*fp == '!' || *fp == '-')
	{
	  not = 1;
	  fp++;
	  if (!*fp)
	    break;
	}

      if (*fp++ != '#')
        continue;

      if (!strcasecmp (client->thd->tls->fp, fp))
	allowed = not ? 0 : 1;
    }

  strv_free (access);
  return allowed ? 0 : GPG_ERR_EACCES;
}

gpg_error_t
tls_validate_access (struct client_s *client, const char *filename)
{
  gpg_error_t rc = do_tls_validate_access (client, "global");

  if (!rc && filename)
    rc = do_tls_validate_access (client, filename);

  return rc;
}

void
tls_rehandshake ()
{
  unsigned t, i;

  MUTEX_LOCK (&cn_mutex);
  t = slist_length (cn_thread_list);
  for (i = 0; i < t; i++)
    {
      struct client_thread_s *thd = slist_nth_data (cn_thread_list, i);

      if (thd->remote && thd->tls)
        thd->tls->rehandshake = 1;
    }
  MUTEX_UNLOCK (&cn_mutex);
}

static int
start_stop_tls_with_protocol (int ipv6, int term)
{
  struct addrinfo hints, *servinfo, *p;
  int port = config_get_integer ("global", "tcp_port");
  char buf[7];
  int n;
  int *fd = ipv6 ? &tls6_fd : &tls_fd;

  if (term || config_get_boolean ("global", "enable_tcp") == 0)
    {
      if (tls6_fd != -1)
	{
	  shutdown (tls6_fd, SHUT_RDWR);
	  close (tls6_fd);
	  tls6_fd = -1;
	}

      if (tls_fd != -1)
	{
	  shutdown (tls_fd, SHUT_RDWR);
	  close (tls_fd);
	  tls_fd = -1;
	}

      return 0;
    }

  memset (&hints, 0, sizeof (hints));
  hints.ai_family = ipv6 ? AF_INET6 : AF_INET;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_flags = AI_PASSIVE;
  snprintf (buf, sizeof (buf), "%i", port);

  if ((n = getaddrinfo (NULL, buf, &hints, &servinfo)) == -1)
    {
      log_write ("getaddrinfo(): %s", gai_strerror (n));
      return 0;
    }

  for (n = 0, p = servinfo; p != NULL; p = p->ai_next)
    {
      int r = 1;

      if ((ipv6 && p->ai_family != AF_INET6)
	  || (!ipv6 && p->ai_family != AF_INET))
	continue;

      if ((*fd = socket (p->ai_family, p->ai_socktype, p->ai_protocol)) == -1)
	{
	  log_write ("socket(): %s", strerror (errno));
	  continue;
	}

      if (setsockopt (*fd, SOL_SOCKET, SO_REUSEADDR, &r, sizeof (int)) == -1)
	{
	  log_write ("setsockopt(): %s",
		     pwmd_strerror (gpg_error_from_errno (errno)));
	  freeaddrinfo (servinfo);
	  goto fail;
	}

      if (bind (*fd, p->ai_addr, p->ai_addrlen) == -1)
	{
	  close (*fd);
	  log_write ("bind(): port %u: %s", port,
		     pwmd_strerror (gpg_error_from_errno (errno)));
	  continue;
	}

      n++;
      break;
    }

  freeaddrinfo (servinfo);

  if (!n)
    goto fail;

#if HAVE_DECL_SO_BINDTODEVICE != 0
  char *tmp = config_get_string ("global", "tcp_interface");
  if (tmp && setsockopt (*fd, SOL_SOCKET, SO_BINDTODEVICE, tmp,
			 strlen (tmp)) == -1)
    {
      log_write ("setsockopt(): %s",
		 pwmd_strerror (gpg_error_from_errno (errno)));
      xfree (tmp);
      goto fail;
    }

  xfree (tmp);
#endif

  if (listen (*fd, 128) == -1)
    {
      log_write ("listen(): %s", strerror (errno));
      goto fail;
    }

  return 1;

fail:
  start_stop_tls_with_protocol (0, 1);
  if (tls_fd != -1)
    close (tls_fd);

  if (tls6_fd != -1)
    close (tls6_fd);

  tls_fd = -1;
  tls6_fd = -1;
  return 0;
}

int
tls_start_stop (int term)
{
  char *s = config_get_string ("global", "tcp_bind");
  int b;

  if (!s)
    return 0;

  if (!strcmp (s, "any"))
    {
      b = start_stop_tls_with_protocol (0, term);
      if (b)
	b = start_stop_tls_with_protocol (1, term);
    }
  else if (!strcmp (s, "ipv4"))
    b = start_stop_tls_with_protocol (0, term);
  else if (!strcmp (s, "ipv6"))
    b = start_stop_tls_with_protocol (1, term);
  else
    b = 0;

  xfree (s);
  if (!term && b)
    {
      gpg_error_t rc = tls_init_params ();
      if (rc)
        {
          start_stop_tls_with_protocol (0, 1);
          return 0;
        }
    }

  return b;
}
