/*
    Copyright (C) 2006-2021 Ben Kibbey <bjk@luxsci.net>

    This file is part of pwmd.

    Pwmd is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    Pwmd is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Pwmd.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef ACL_H
#define ACL_H

gpg_error_t do_validate_peer (assuan_context_t ctx, const char *section,
                              assuan_peercred_t *peer, char **rpath);
gpg_error_t peer_is_invoker(struct client_s *client);
gpg_error_t acl_check_common (struct client_s *client, const char *user,
                              uid_t uid, gid_t gid, int *allowed);
gpg_error_t validate_peer (struct client_s *);

#endif
