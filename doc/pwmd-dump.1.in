.\" Copyright (C) 2015, 2016
.\" Ben Kibbey <bjk@luxsci.net>
.\" 
.\" This file is part of pwmd.
.\" 
.\" Pwmd is free software: you can redistribute it and/or modify
.\" it under the terms of the GNU General Public License as published by
.\" the Free Software Foundation, either version 2 of the License, or
.\" (at your option) any later version.
.\" 
.\" Pwmd is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU General Public License for more details.
.\" 
.\" You should have received a copy of the GNU General Public License
.\" along with pwmd.  If not, see <http://www.gnu.org/licenses/>.
.de URL
\\$2 \(laURL: \\$1 \(ra\\$3
..
.if \n[.g] .mso www.tmac
.TH PWMD-DUMP 1 "27 Jul 2016" "@VERSION@" "Password Manager Daemon"
.SH NAME

pwmd-dump \- dump raw XML from a pwmd 3.0.x data file
.SH SYNOPSIS
.B pwmd-dump
[options] <infile> <outfile>

.SH DESCRIPTION
.B pwmd-dump
reads a
.BR pwmd (1)
version 3.0.x data file and dumps the raw unencrypted XML to the specified
output file while stripping the literal element character and non-reserved
attributes and child nodes of an element with a
.B target
attribute. See option
.B --no-convert.
.P
If
.B <outfile>
is
.B "-"
then the output will be written to standard output. Otherwise, 
.B <outfile>
must not already exist.
.P
It is recommended to securely delete the resulting XML file after importing to
prevent recovering. There are a few tools that can do this:
.BR wipe (1)
,
.BR srm (1)
,
.BR shred (1)
and others.

.SH OPTIONS
.TP
.I "\--no-convert, -n"
Before dumping the XML,
.B pwmd-dump
will strip any leading literal element character '!' from
each element in the element path of a "target" attribute and remove all
non-reserved attributes and all child nodes of the element containing
the "target" attribute. Since version 3.1 of
.B pwmd
there are no such literal elements. All targets are followed just as a
symbolic link on a filesystem. This option prevents converting and will dump
the XML unmodified.

.TP
.I "\--force"
Do not abort conversion and force removal of content and children of an
element with a "target" attribute.

.TP
.I "\--xml"
The file to read is a raw unencrypted
.B pwmd
XML document.

.TP
.I "\--keyfile <filename>"
Obtain the passphrase to use to decrypt the data file from the specified
filename.

.TP
.I "\--version"
Show version information.

.TP
.I "\--help"
Show help text.

.SH AUTHOR
Ben Kibbey <bjk@luxsci.net>
.br
.URL "https://gitlab.com/bjk/pwmd/wikis" "Pwmd Homepage" .

.SH "SEE ALSO"
.BR pwmd (1)

And the pwmd texinfo manual for protocol commands and their syntax.
