#!/bin/sh
#
# Tests for the BULK protocol command.
#
MAX_TESTS=4

. $AM_SRCDIR/common.sh
. $AM_SRCDIR/import-common.sh

init_tests() {
    do_import datafile
    cat >$OUTDIR/config <<EOF
[global]
log_level=9
#enable_logging=true
[datafile]
passphrase_file=$WDIR/passphrase.key
[datafile.tmp]
passphrase_file=$WDIR/passphrase.key
EOF
    launch_pwmd datafile
}

test_1() {
    cp -f data/datafile data/datafile.tmp || bail_out "Could not copy data/datafile"
    run_pwmc "--inquire 'BULK --inquire' datafile.tmp" $DEVNULL <<EOF
(2:id 1:1 7:getinfo7:vrsion)
EOF
    test_failure $test_n $? 536870941 "Inquire syntax error"
}

test_2() {
    cp -f data/datafile data/datafile.tmp || bail_out "Could not copy data/datafile"
    run_pwmc "--inquire 'BULK --inquire' datafile.tmp" >result $DEVNULL <<EOF
(2:id 1:1 7:getinfo7:clients
    2:rc 1:0
	(2:id1:2 4:list5:--all
	    2:rc 1:0
		(2:id1:3 3:get0:
		    2:rc 9:536870941
			(2:id1:4 7:getinfo7:clients)
		    2:id1:5 7:getinfo5:cache))
	2:id1:6 9:getconfig12:client-state
	    2:rc 9:536871086
		(2:id1:1 4:list0:))
EOF
    test_result $test_n $? bulk "Command branching"
}

test_3() {
    cp -f data/datafile data/datafile.tmp || bail_out "Could not copy data/datafile"
    run_pwmc "--inquire 'BULK --inquire' datafile.tmp" >result $DEVNULL <<EOF
(2:id 1:1 7:getinfo7:clients
    2:rc 1:0
	(2:id1:2 4:list5:--all
	    2:rc 1:0
		(2:id1:3 3:get3:a	b
		    2:rc 9:536870941
			(2:id1:4 3:nop0:)
		    2:rc 1:0
			(2:id1:5 7:getinfo14:--data clients))
	    2:id1:6 9:getconfig3:nop
		2:rc 9:536871086
		    (2:id1:7 4:list0:))
    2:id1:8 7:getinfo 3:nop)
EOF
    test_result $test_n $? bulk "Command branching (next rc)"
}

test_4() {
    cp -f data/datafile data/datafile.tmp || bail_out "Could not copy data/datafile"
    run_pwmc "--inquire 'BULK --inquire' datafile.tmp" >result $DEVNULL <<EOF
(2:id 1:1 7:getinfo7:clients
    2:rc 11:12345|67890
	(2:id1:2 4:list5:--all
	    2:rc 1:0
		(2:id1:3 3:get3:a	b
		    2:rc 9:536870941
			(2:id1:4 3:nop0:)
		    2:rc 1:0
			(2:id1:5 7:getinfo14:--data clients))
	    2:id1:6 9:getconfig3:nop
		2:rc 9:536871086
		    (2:id1:7 4:list0:))
    2:rc 1:0
	(2:id1:8 7:getinfo 3:nop))
EOF
    test_result $test_n $? bulk "Command branching (multiple rc's)"
}

run_tests $@
